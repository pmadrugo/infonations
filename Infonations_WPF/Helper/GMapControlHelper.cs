﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using GMap.NET;
using GMap.NET.WindowsPresentation;
using Infonations.Services;

namespace Infonations_WPF.Helper
{
    /// <summary>
    /// Extension/Variation of the GMapControl object from the GMaps.Net NuGet
    /// </summary>
    public class GMapControlHelper : GMapControl
    {
        /// <summary>
        /// Constructor, sets the path to save the map database along with checking if there is internet connection or not so it can choose what cache to use
        /// </summary>
        public GMapControlHelper() : base()
        {
            var mapDataPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\MapCache\";
            this.CacheLocation = mapDataPath;

            if (!NetworkService.CheckConnection().IsSuccess)
            {
                this.Manager.Mode = AccessMode.CacheOnly;
            }
            else
            {
                this.Manager.Mode = AccessMode.ServerAndCache;
            }
        }

        /// <summary>
        /// An abstraction of the underlying Position property present in the object, through this, one can use it as a DependencyProperty
        /// </summary>
        public PointLatLng Position
        {
            get
            {
                return (PointLatLng)(GetValue(PositionProperty));
            }
            set
            {
                base.Position = Position;
                SetValue(PositionProperty, value);
            }
        }
        
        /// <summary>
        /// Position Dependency Property - Communicates with the original NuGet property through a property that communicates with the underlying value
        /// </summary>
        public static readonly DependencyProperty PositionProperty = DependencyProperty.Register(
            "Position", 
            typeof(PointLatLng), 
            typeof(GMapControlHelper), 
            new PropertyMetadata(
                new PropertyChangedCallback(PositionPropertyChanged)
            )
        );

        private static void PositionPropertyChanged(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            PointLatLng temp = (PointLatLng)e.NewValue;

            GMapControlHelper mapControl = source as GMapControlHelper;
            if (mapControl != null)
            {
                mapControl.Position = new PointLatLng(temp.Lat, temp.Lng);
            }
        }
    }
}
