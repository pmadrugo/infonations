﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Models.Country;
using Caliburn.Micro;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;

namespace Infonations_WPF.Helper
{
    /// <summary>
    /// Extended variation of the TimeZoneModel possessing Notification abilities
    /// </summary>
    public class NotifiableTimeZoneModel : TimeZoneModel, INotifyPropertyChanged
    {
        private DispatcherTimer _timer;
        private string _tzString;

        /// <summary>
        /// String representing the Timezone value
        /// </summary>
        public string TZString
        {
            get
            {
                return _tzString;
            }
            set
            {
                _tzString = value;
                NotifyPropertyChanged("TZString");
                
            }
        }

        /// <summary>
        /// Delegate - Represents the on property change event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        /// <summary>
        /// Constructor - Deploys a timer to update it at every second (akin to a clock)
        /// </summary>
        public NotifiableTimeZoneModel()
        {
            TZString = "Loading...";

            _timer = new DispatcherTimer(DispatcherPriority.Background);
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.IsEnabled = true;
            _timer.Tick += (s, e) =>
            {
                TZString = this.TZDateTimeNow.ToLongTimeString();
            };
        }
    }
}
