﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Infonations_WPF.Models
{
    /// <summary>
    /// Base model for the Chuck Norris joke API
    /// </summary>
    public class CNJBaseModel
    {
        /// <summary>
        /// Success or not
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }

        /// <summary>
        /// What joke was received
        /// </summary>
        [JsonProperty("value")]
        public CNJokeModel Value { get; set; }
    }
}
