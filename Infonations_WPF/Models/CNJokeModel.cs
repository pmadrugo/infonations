﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Infonations_WPF.Models
{
    /// <summary>
    /// Model for the Chuck Norris joke
    /// </summary>
    public class CNJokeModel
    {
        /// <summary>
        /// ID
        /// </summary>
        [JsonProperty("id")]
        public int ID { get; set; }

        /// <summary>
        /// The joke itself
        /// </summary>
        [JsonProperty("joke")]
        public string Joke { get; set; }

        /// <summary>
        /// Categories to which the joke belongs
        /// </summary>
        [JsonProperty("categories")]
        public object Categories { get; set; }
    }
}
