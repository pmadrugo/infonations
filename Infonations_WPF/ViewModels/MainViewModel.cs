﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Caliburn.Micro;
using Infonations;
using Infonations.Models.Common;
using Infonations.Models.Country;
using Infonations_WPF.Views;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// Main ViewModel
    /// </summary>
    public class MainViewModel : Conductor<object>
    {
        #region Constructor

        /// <summary>
        /// Main ViewModel constructor - Sets up the Progress object along with the CancellationTokenSource, starts up the loading process
        /// </summary>
        public MainViewModel()
        {
            this._isLoaded = false;

            this.Progress = new Progress<ResponseModel>();
            this.Progress.ProgressChanged += Progress_ReportReceived;

            this.CTS = new CancellationTokenSource();
            
            this.LoadCountries();
        }

        #endregion

        #region Attributes
        private bool _isLoaded;
        private BindableCollection<CountryModel> _countries;
        private BindableCollection<RegionModel> _regions;
        private BindableCollection<CurrencyModel> _currencies;
        private BindableCollection<LanguageModel> _languages;

        private string _countriesFilter;
        private CountryModel _selectedCountry;
        private string _logText;

        private int _progressBarLoading;

        private string _textLoading;
        #endregion

        #region Properties
        /// <summary>
        /// Property related to the visibility of the placeholder in the text field for the country search bar
        /// </summary>
        public Visibility ShowPlaceholder
        {
            get
            {
                if (string.IsNullOrEmpty(CountriesFilter))
                {
                    return Visibility.Visible;
                }
                return Visibility.Hidden;
            }
        }

        /// <summary>
        /// Property related to the visibility of the content pane (When loading, hides)
        /// </summary>
        public Visibility ShowContent
        {
            get
            {
                if (_isLoaded)
                {
                    return Visibility.Visible;
                }
                return Visibility.Hidden;
            }
        }

        /// <summary>
        /// Property related to the visibility of the loading controls (When loading, shows)
        /// </summary>
        public Visibility ShowLoading
        {
            get
            {
                if (!_isLoaded)
                {
                    return Visibility.Visible;
                }
                return Visibility.Hidden;
            }
        }

        /// <summary>
        /// Progress object
        /// </summary>
        public Progress<ResponseModel> Progress { get; set; }

        /// <summary>
        /// Cancellation Token Source
        /// </summary>
        public CancellationTokenSource CTS { get; set; }

        /// <summary>
        /// String representing the log textbox
        /// </summary>
        public string LogText
        {
            get { return _logText; }
            set {
                _logText = value;
                NotifyOfPropertyChange(() => LogText);
            }
        }

        /// <summary>
        /// Collection holding the countries
        /// </summary>
        public BindableCollection<CountryModel> Countries
        {
            get
            {
                if (_isLoaded)
                {
                    if (!string.IsNullOrEmpty(CountriesFilter))
                    {
                        return new BindableCollection<CountryModel>(
                            from country in _countries
                            where CountriesFilter.ToLower().Split(' ').Any((filter) => country.Name.ToLower().Contains(filter))
                            select country
                        );
                    }
                    return _countries;
                }
                return null;
            }
            set
            {
                this._countries = value;
                NotifyOfPropertyChange(() => Countries);
            }
        }

        /// <summary>
        /// Collection holding the regions
        /// </summary>
        public BindableCollection<RegionModel> Regions
        {
            get { return _regions; }
            set {
                _regions = value;
                NotifyOfPropertyChange(() => Regions);
            }
        }


        /// <summary>
        /// Collection holding the languages
        /// </summary>
        public BindableCollection<LanguageModel> Languages
        {
            get { return _languages; }
            set
            {
                _languages = value;
                NotifyOfPropertyChange(() => Languages);
            }
        }

        /// <summary>
        /// Collection holding the currencies
        /// </summary>
        public BindableCollection<CurrencyModel> Currencies
        {
            get { return _currencies; }
            set
            {
                _currencies = value;
                NotifyOfPropertyChange(() => Currencies);
            }
        }

        /// <summary>
        /// String representing the country search bar
        /// </summary>
        public string CountriesFilter
        {
            get { return _countriesFilter; }
            set {
                _countriesFilter = value;

                NotifyOfPropertyChange(() => CountriesFilter);
                NotifyOfPropertyChange(() => ShowPlaceholder);
                NotifyOfPropertyChange(() => Countries);
            }
        }

        /// <summary>
        /// The currently selected country
        /// </summary>
        public CountryModel SelectedCountry
        {
            get { return _selectedCountry; }
            set {
                _selectedCountry = value;

                NotifyOfPropertyChange(() => SelectedCountry);
                NotifyOfPropertyChange(() => CanCountryDetails);
                NotifyOfPropertyChange(() => CanHolidaysListing);
                NotifyOfPropertyChange(() => CanCountryMap);

                if (SelectedCountry != null)
                {
                    CountryDetails();
                }
            }
        }

        /// <summary>
        /// Property informing if the About button is enabled or not
        /// </summary>
        public bool CanAboutWindow
        {
            get
            {
                return _isLoaded;
            }
        }

        /// <summary>
        /// Property holding the current progress bar value
        /// </summary>
        public int ProgressBarLoading
        {
            get { return _progressBarLoading; }
            set
            {
                _progressBarLoading = value;
                NotifyOfPropertyChange(() => ProgressBarLoading);
            }
        }

        /// <summary>
        /// Property holding the current progress text
        /// </summary>
        public string TextLoading
        {
            get { return _textLoading; }
            set
            {
                _textLoading = value;
                NotifyOfPropertyChange(() => TextLoading);
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loading countries task
        /// </summary>
        /// <returns></returns>
        public async Task LoadCountries()
        {
            InfonationsData infonationsData = new InfonationsData(@".\Data");
                    
            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            await infonationsData.LoadData(this.Progress, this.CTS.Token);

            stopwatch.Stop();

            this.LogText = $"Data loading finished... {stopwatch.ElapsedMilliseconds/1000}s\n{this.LogText}";
                    
            this.Countries = new BindableCollection<CountryModel>(infonationsData.Countries.OrderBy((c) => c.Name));
            //this.Regions = new BindableCollection<RegionModel>(infonationsData.Regions.OrderBy((r) => r.RegionName).ThenBy((r) => r.Countries.OrderBy((c) => c.Name)));
            this.Currencies = new BindableCollection<CurrencyModel>(infonationsData.Currencies.OrderBy((currency) => currency.Name));
            //this.Languages = new BindableCollection<LanguageModel>(infonationsData.Languages.OrderBy((language) => language.Name));
            
            OnLoad();
        }

        #endregion

        #region Events
        /// <summary>
        /// On Progress getting a report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Progress_ReportReceived(object sender, ResponseModel e)
        {
            OnUIThread(
                () =>
                {
                    switch (e.Message)
                    {
                        case string message when e.Message.Contains("Loader|"):
                            {

                                this.TextLoading = e.Message.Remove(0, $"Loader|".Length);
                                this.ProgressBarLoading = Convert.ToInt32(e.Result);
                                return;
                            }
                        default:
                            {
                                OnUIThread(() => this.LogText = $"{e.Message}\n{this.LogText}");
                                return;
                            }
                    }
                }
            );
        }
        #endregion

        #region Controls

        #region Country Details Button
        /// <summary>
        /// Action to access the country details pane
        /// </summary>
        public void CountryDetails()
        {
            ActivateItem(new CountryDetailsViewModel(this.Progress, this.CTS.Token, this.SelectedCountry, this.Currencies, this));
        }

        /// <summary>
        /// Property indicating if one can access the country details
        /// </summary>
        public bool CanCountryDetails
        {
            get
            {
                if (this.SelectedCountry != null)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region Holidays Listing Button

        /// <summary>
        /// Action to access the holidays listing pane
        /// </summary>
        public void HolidaysListing()
        {
            ActivateItem(new HolidaysListingViewModel(this.Progress, this.CTS.Token, this.SelectedCountry));
        }

        /// <summary>
        /// Property indicating if one can access the holidays listing
        /// </summary>
        public bool CanHolidaysListing
        {
            get
            {
                if (this.SelectedCountry != null)
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Country Map
        /// <summary>
        /// Action to access the map pane
        /// </summary>
        public void CountryMap()
        {
            ActivateItem(new CountryMapViewModel(this.Progress, this.CTS.Token, this.SelectedCountry));
        }

        /// <summary>
        /// Property indicating if one can access the map
        /// </summary>
        public bool CanCountryMap
        {
            get
            {
                if (this.SelectedCountry != null)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Action to access the About Window
        /// </summary>
        public void AboutWindow()
        {
            IWindowManager manager = new WindowManager();

            var aboutWindowViewModel = new AboutWindowViewModel();

            manager.ShowWindow(aboutWindowViewModel, null, null);

            AboutWindowView aboutWindowView = (AboutWindowView)aboutWindowViewModel.GetView();

            aboutWindowView.Deactivated += delegate 
            {
                if (aboutWindowViewModel.IsActive)
                {
                    aboutWindowViewModel.IsActive = false;
                    aboutWindowViewModel.TryClose();
                }
            };
            aboutWindowView.Topmost = true;
        }

        #endregion

        #region Events - View

        /// <summary>
        /// View OnLoad event
        /// </summary>
        public void OnLoad()
        {
            MainView mainView = (MainView)this.GetView();
            
            mainView.Cursor = Cursors.Arrow;

            this._isLoaded = true;

            //this.LogText = $"{this.Countries.Count}\n{this.LogText}";
            //this.LogText = $"{this.Currencies.Count}\n{this.LogText}";

            NotifyOfPropertyChange(() => this.Countries);
            NotifyOfPropertyChange(() => CanAboutWindow);
            NotifyOfPropertyChange(() => ShowContent);
            NotifyOfPropertyChange(() => ShowLoading);
            
            if (this.Countries.Count == 0)
            {
                IWindowManager windowManager = new WindowManager();

                var noInternet = new NoInternetViewModel();

                windowManager.ShowDialog(noInternet, null, null);
            }
        }
        #endregion
    }
}
