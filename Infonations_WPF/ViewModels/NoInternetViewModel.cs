﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Caliburn.Micro;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// ViewModel for the dialog indicating there is no internet connection
    /// </summary>
    public class NoInternetViewModel : Screen
    {
        /// <summary>
        /// NoInternet ViewModel
        /// </summary>
        public NoInternetViewModel() { }
        
        /// <summary>
        /// Action when user presses Understood (Closes the application)
        /// </summary>
        public void Understood()
        {
            Application.Current.Shutdown();
        }
    }
}
