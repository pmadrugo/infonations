﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Infonations;
using Infonations.Models.Common;
using Infonations.Models.Country;
using Infonations.Services;
using Infonations_WPF.Helper;
using Infonations_WPF.Views;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// Country Details ViewModel
    /// </summary>
    public class CountryDetailsViewModel : Screen
    {
        #region Attributes
        IWindowManager _manager;

        private CountryModel _selectedCountry;
        private CountryModel _selectedBorderCountry;

        private BindableCollection<CurrencyModel> _otherCurrencies;

        private string _currentCurrencyValue;
        private double _currentCurrencyDoubleValue;

        private string _otherCurrencyValue;

        private CurrencyModel _selectedCurrentCurrency;
        private CurrencyModel _selectedOtherCurrency;

        private BindableCollection<NotifiableTimeZoneModel> _notifiableTimeZones;

        private string _selectedCountryHello;

        private bool _anthemDownloaded;

        #endregion

        #region Properties
        /// <summary>
        /// Property holding bindable timezones
        /// </summary>
        public BindableCollection<NotifiableTimeZoneModel> NotifiableTimeZones
        {
            get { return _notifiableTimeZones; }
            set
            {
                _notifiableTimeZones = value;
                NotifyOfPropertyChange(() => NotifiableTimeZones);
            }
        }

        /// <summary>
        /// The currently selected country
        /// </summary>
        public CountryModel SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                NotifyOfPropertyChange(() => SelectedCountry);
                NotifyOfPropertyChange(() => CountryBorders);
            }
        }

        /// <summary>
        /// The currently selected country in the Bordering Countries section
        /// </summary>
        public CountryModel SelectedBorderCountry
        {
            get { return _selectedBorderCountry; }
            set
            {
                _selectedBorderCountry = value;
                NotifyOfPropertyChange(() => SelectedBorderCountry);
                NotifyOfPropertyChange(() => CanGoToBorderCountry);
            }
        }

        /// <summary>
        /// The currently selected currency in the Currency Converter (Selected Country)
        /// </summary>
        public CurrencyModel SelectedCurrentCurrency
        {
            get { return _selectedCurrentCurrency; }
            set
            {
                _selectedCurrentCurrency = value;
                NotifyOfPropertyChange(() => SelectedCurrentCurrency);
            }
        }

        /// <summary>
        /// The currently selected currency in the Currency Converter (All Currencies)
        /// </summary>
        public CurrencyModel SelectedOtherCurrency
        {
            get { return _selectedOtherCurrency; }
            set
            {
                _selectedOtherCurrency = value;
                NotifyOfPropertyChange(() => SelectedOtherCurrency);
                NotifyOfPropertyChange(() => OtherCurrencyValue);
            }
        }
        
        /// <summary>
        /// The current currency value (Selected Country)
        /// </summary>
        public string CurrentCurrencyValue
        {
            get
            {
                return _currentCurrencyValue;
            }
            set
            {
                _currentCurrencyValue = value;
                
                double aux;
                if (double.TryParse(value, out aux) && (aux >= 0))
                {
                    _currentCurrencyDoubleValue = aux;
                    NotifyOfPropertyChange(() => OtherCurrencyValue);
                }
                NotifyOfPropertyChange(() => CurrentCurrencyValue);
            }
        }

        /// <summary>
        /// The currency value of the selected currency amongst all currencies in relation to the selected country's currency
        /// </summary>
        public string OtherCurrencyValue
        {
            get
            {
                if (SelectedCurrentCurrency != null && SelectedOtherCurrency != null)
                {
                    if (SelectedCurrentCurrency.Rate != 0 && SelectedOtherCurrency.Rate != 0)
                    {
                        return ((decimal)_currentCurrencyDoubleValue / (decimal)SelectedCurrentCurrency.Rate * (decimal)SelectedOtherCurrency.Rate).ToString("N2") + $" {SelectedOtherCurrency.Symbol}";
                    }
                    else
                    {
                        return $"Sorry, rate not found!";
                    }
                }
                return "";
            }
            set
            {
                _otherCurrencyValue = value;
                NotifyOfPropertyChange(() => OtherCurrencyValue);
            }
        }

        /// <summary>
        /// List of countries that border the current one
        /// </summary>
        public List<CountryModel> CountryBorders
        {
            get
            {
                if (SelectedCountry.BorderCountries != null)
                {
                    return SelectedCountry.BorderCountries.Cast<CountryModel>().ToList();
                }
                return null;
            }
        }

        /// <summary>
        /// List of all existing currencies
        /// </summary>
        public BindableCollection<CurrencyModel> OtherCurrencies
        {
            get
            {
                return _otherCurrencies;
            }
            set
            {
                _otherCurrencies = value;
                NotifyOfPropertyChange(() => OtherCurrencies);
            }
        }

        /// <summary>
        /// The Hello string for the currently selected country
        /// </summary>
        public string SelectedCountryHello
        {
            get { return _selectedCountryHello;  }
            set
            {
                _selectedCountryHello = value;
                NotifyOfPropertyChange(() => SelectedCountryHello);
            }
        }

        /// <summary>
        /// Property indicating if one can use the Go to Country button in the Bordering Countries
        /// </summary>
        public bool CanGoToBorderCountry
        {
            get
            {
                if (SelectedBorderCountry != null)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Property indicating if the anthem has been downloaded or is present in the folder
        /// </summary>
        public bool AnthemDownloaded
        {
            get { return _anthemDownloaded; }
            set
            {
                _anthemDownloaded = value;
                NotifyOfPropertyChange(() => AnthemDownloaded);
                NotifyOfPropertyChange(() => CanAnthem);
            }
        }

        /// <summary>
        /// Property indicating if one can open the media player to listen to the anthem
        /// </summary>
        public bool CanAnthem
        {
            get
            {
                return AnthemDownloaded;
            }
        }

        /// <summary>
        /// Property to point to the Main ViewModel
        /// </summary>
		public MainViewModel MainViewModel{ get; set; }

        #endregion

        #region Constructor
        /// <summary>
        /// Country Details ViewModel - Sets up the currently selected country values, and calls up the HelloSalut API along with checking if the anthem is present or requires download
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">The currently selected country</param>
        /// <param name="Currencies">List of all currencies</param>
        /// <param name="mainView">Main ViewModel</param>
        public CountryDetailsViewModel(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry, BindableCollection<CurrencyModel> Currencies, MainViewModel mainView)
        {
            this.NotifiableTimeZones = new BindableCollection<NotifiableTimeZoneModel>();

            this._manager = new WindowManager();
            this.MainViewModel = mainView;

            this.SelectedCountry = SelectedCountry;
            this.OtherCurrencies = Currencies;

            BuildLocalTZModels(progress, cToken);

            SayHello(progress, cToken);

            GetCountryAnthem(progress, cToken);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Acquires the selected country's Hello text
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        private async void SayHello(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            InfonationsData dataAccess = new InfonationsData(@".\Data");

            await dataAccess.GetHelloData(progress, cToken, this.SelectedCountry);

            SelectedCountryHello = SelectedCountry.Hello;
        }

        /// <summary>
        /// Assembles the timezones according to the bindable model
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        private void BuildLocalTZModels(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            List<NotifiableTimeZoneModel> notifyTzModels = new List<NotifiableTimeZoneModel>();
            foreach (TimeZoneModel tzModel in this.SelectedCountry.TimeZones)
            {
                NotifiableTimeZoneModel newTZ = new NotifiableTimeZoneModel()
                {
                    BaseDifference = tzModel.BaseDifference,
                    Designation = tzModel.Designation,
                    Sign = tzModel.Sign
                };

                notifyTzModels.Add(
                    newTZ
                );
            }
            this.NotifiableTimeZones.AddRange(notifyTzModels);
        }

        /// <summary>
        /// Action that sends the user towards the currently selected border country
        /// </summary>
        public void GoToBorderCountry()
        {
            this.SelectedCountry = this.SelectedBorderCountry;
            NotifyOfPropertyChange(() => this.SelectedCountry);
			
			// BEGIN Handling parent view
			MainView mainView = this.MainViewModel.GetView() as MainView;
			
            if (mainView != null)
            {
                mainView.CountriesListBox.SelectedItem = this.SelectedCountry;
                mainView.CountriesListBox.ScrollIntoView(mainView.CountriesListBox.SelectedItem);
            }			
			// END Handling parent view					 
        }

        /// <summary>
        /// Gets the selected country's anthem
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        private void GetCountryAnthem(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            var anthemTask = Task.Run(
                () =>
                {
                    var result = InfonationsData.GetCountryAnthem(progress, cToken, SelectedCountry,
                        new System.ComponentModel.AsyncCompletedEventHandler(
                           (o, e) =>
                           {
                               AnthemDownloaded = true;
                           }
                        )
                    );

                    if (result.IsSuccess && result.Result != null)
                    {
                        AnthemDownloaded = true;
                        SelectedCountry.AnthemPath = (string)result.Result;
                    }
                    else
                    {
                        AnthemDownloaded = false;
                    }
                }
            );
        }
        
        /// <summary>
        /// Action opening up the media player for the selected country's anthem
        /// </summary>
        public void Anthem()
        {
            this._manager.ShowDialog(new AnthemMediaPlayerViewModel(this.SelectedCountry.AnthemPath), null, null);
        }

        /// <summary>
        /// Action zooming in the selected country's flag
        /// </summary>
        public void ImageZoom()
        {
            var imageZoomViewModel = new ImageZoomViewModel(this.SelectedCountry.FlagPath);

            this._manager.ShowWindow(imageZoomViewModel, null, null);

            ImageZoomView imageZoomView = (ImageZoomView)imageZoomViewModel.GetView();

            imageZoomView.Deactivated += delegate {
                if (imageZoomViewModel.IsActive)
                {
                    imageZoomViewModel.IsActive = false;
                    imageZoomViewModel.TryClose();
                }
            };
            imageZoomView.Topmost = true;
        }
        #endregion

    }
}
