﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using Infonations_WPF.Views;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// ImageZoom ViewModel - Form with a zoomed up version of the flag
    /// </summary>
    public class ImageZoomViewModel : Screen
    {
        private string _imageURL;

        /// <summary>
        /// Property indicating if the form is to be closed or not (Activated or not)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// String indicating the path towards the flag
        /// </summary>
        public string ImageURL
        {
            get { return _imageURL; }
            set
            {
                _imageURL = value;
                NotifyOfPropertyChange(() => ImageURL);
            }
        }

        /// <summary>
        /// Constructor indicating that the flag is currently in use, along with setting the image path automatically towards the received flag path
        /// </summary>
        /// <param name="flagPath">Path towards the flag/image to show</param>
        public ImageZoomViewModel(string flagPath)
        {
            this.IsActive = true;
            this.ImageURL = flagPath;
        }

        /// <summary>
        /// Method to indicate that the user clicked anywhere in the form
        /// </summary>
        public void ClickAnywhere()
        {
            if (IsActive)
            {
                IsActive = false;
                this.TryClose();
            }
        }
    }
}
