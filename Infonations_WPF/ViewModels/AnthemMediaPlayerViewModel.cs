﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Caliburn.Micro;
using Infonations_WPF.Views;

namespace Infonations_WPF.ViewModels
{
    public class AnthemMediaPlayerViewModel : Screen
    {
        #region Attributes
        private string _mp3Title;
        #endregion

        #region Properties
        /// <summary>
        /// Boolean indicating the media player state. True if playing, false if paused, null if stopped
        /// </summary>
        public bool? Playing { get; set; }

        /// <summary>
        /// URI indicating the path towards the anthem
        /// </summary>
        public Uri AnthemPath { get; set; }

        /// <summary>
        /// The title of the MP3 file
        /// </summary>
        public string MP3Title
        {
            get { return _mp3Title; }
            set
            {
                _mp3Title = value;
                NotifyOfPropertyChange(() => MP3Title);
            }
        }

        /// <summary>
        /// Property to indicate if one can stop playing
        /// </summary>
        public bool CanStop
        {
            get
            {
                if (this.Playing != null)
                {
                    return true;
                }
                return false;
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// ViewModel constructor, sets up the anthem path, along with starting up the anthem by default and setting the title
        /// </summary>
        /// <param name="anthemPath">String containing the Anthem path</param>
        public AnthemMediaPlayerViewModel(string anthemPath)
        {
            this.AnthemPath = new Uri(anthemPath, UriKind.Relative);
            this.Playing = true;

            var tfile = TagLib.File.Create(anthemPath);
            if (tfile != null)
            {
                MP3Title = tfile.Tag.Title;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// OnLoad event - Starts up the anthem
        /// </summary>
        public void UserControlLoad()
        {
            var viewKeyValue = this.Views.ElementAt(0).Value;
            if (viewKeyValue != null)
            {
                AnthemMediaPlayerView view = (AnthemMediaPlayerView)viewKeyValue;
                view.mediaPlayer.Play();
            }
        }

        /// <summary>
        /// Starts up the media player
        /// </summary>
        public void Play()
        {
            var viewKeyValue = this.Views.ElementAt(0).Value;
            if (viewKeyValue != null)
            {
                AnthemMediaPlayerView view = (AnthemMediaPlayerView)viewKeyValue;
                if (Playing == false || Playing == null)
                {
                    view.mediaPlayer.Play();
                    this.Playing = true;
                }
                else
                {
                    if (Playing == true)
                    {
                        view.mediaPlayer.Pause();
                        this.Playing = false;
                    }
                }
            }
        }

        /// <summary>
        /// Stops the media player
        /// </summary>
        public void Stop()
        {
            var viewKeyValue = this.Views.ElementAt(0).Value;
            if (viewKeyValue != null)
            {
                AnthemMediaPlayerView view = (AnthemMediaPlayerView)viewKeyValue;
                if (Playing != null)
                {
                    view.mediaPlayer.Stop();
                    this.Playing = null;
                }
            }
        }

        public void UnloadMediaPlayer(object sender)
        {
            var mediaPlayer = (MediaElement)sender;

            mediaPlayer.Stop();
        }

        /// <summary>
        /// Attempts to close the form
        /// </summary>
        public void Close()
        {
            this.TryClose();
        }
        #endregion
    }
}
