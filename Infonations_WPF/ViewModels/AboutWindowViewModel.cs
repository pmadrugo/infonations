﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Infonations.Models.Common;
using Infonations.Services;
using Infonations_WPF.Models;
using Infonations_WPF.Views;
using Newtonsoft.Json;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// About ViewModel
    /// </summary>
    public class AboutWindowViewModel : Screen
    {
        APIService _apiService;

        private string _apiBaseURL;
        private string _apiControllerURL;

        private string _firstName;
        private string _lastName;

        private string _joke;

        /// <summary>
        /// Property - Joke value
        /// </summary>
        public string Joke
        {
            get { return _joke; }
            set
            {
                _joke = value;
                NotifyOfPropertyChange(() => Joke);
            }
        }

        /// <summary>
        /// Property indicating if the form is to be closed or not (Activated or not)
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// About ViewModel constructor
        /// </summary>
        public AboutWindowViewModel()
        {
            IsActive = true;

            _firstName = "Jonas";
            _lastName = "Pistolas";

            _apiBaseURL = "http://api.icndb.com/";
            _apiControllerURL = $@"jokes/random?escape = javascript&firstName={_firstName}&lastName={_lastName}";

            if (NetworkService.CheckConnectionTo(_apiBaseURL).IsSuccess)
            {
                LoadAPI();
            }
        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        #region Methods

        private async Task LoadAPI()
        {
            _apiService = new APIService(@".\Data");

            Progress<ResponseModel> progress = new Progress<ResponseModel>();
            CancellationTokenSource cTokenSource = new CancellationTokenSource();

            var apiResult = await _apiService.GetJSONAPI(_apiBaseURL, _apiControllerURL, progress, cTokenSource.Token);

            if (apiResult.IsSuccess)
            {
                string data = (string)apiResult.Result;
                CNJBaseModel cnjBaseModel = JsonConvert.DeserializeObject<CNJBaseModel>(data);

                this.Joke = WebUtility.HtmlDecode(cnjBaseModel.Value.Joke);
            }
        }

        /// <summary>
        /// Method to indicate that the user clicked anywhere in the form
        /// </summary>
        public void ClickAnywhere()
        {
            if (IsActive)
            {
                this.IsActive = false;
                this.TryClose();
            }
        }
        #endregion
    }
}
