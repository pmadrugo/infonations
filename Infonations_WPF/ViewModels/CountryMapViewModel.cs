﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Infonations.Models.Common;
using Infonations.Models.Country;
using GMap.NET.WindowsPresentation;
using GMap.NET.MapProviders;
using GMap.NET;
using Caliburn.Micro;
using Infonations_WPF.Helper;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// Country Map ViewModel - User control containing a map
    /// </summary>
    public class CountryMapViewModel : Screen
    {
        #region Attributes

        private CountryModel _selectedCountry;

        #endregion

        #region Properties
        /// <summary>
        /// Currently selected country
        /// </summary>
        public CountryModel SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                if (SelectedCountry != null)
                {
                    NotifyOfPropertyChange(() => MapLatLng);
                }
            }
        }

        /// <summary>
        /// Creates an object that's GMap.Net control compatible using the coordinates.
        /// </summary>
        public PointLatLng MapLatLng
        {
            get
            {
                var currentPoint = new PointLatLng(
                    this.SelectedCountry.Coordinates.Latitude,
                    this.SelectedCountry.Coordinates.Longitude
                );

                return currentPoint;
            }
        }

        /// <summary>
        /// Property to communicate through the XAML to the GMap.Net control
        /// </summary>
        public OpenStreetMapProvider OSMProvider
        {
            get
            {
                return GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
            }
        }


        #endregion

        #region Constructor
        /// <summary>
        /// Country Map ViewModel constructor -- Sets the currently selected country
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">Selected Country</param>
        public CountryMapViewModel(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry)
        {
            this.SelectedCountry = SelectedCountry;
        }

        #endregion

        #region Methods
        /// <summary>
        /// Loads the map
        /// </summary>
        /// <param name="MapView"></param>
        public void LoadMap(GMapControlHelper MapView)
        {
            MapView.ReloadMap();
        }
        #endregion
    }
}
