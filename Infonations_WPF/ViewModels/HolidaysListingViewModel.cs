﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Caliburn.Micro;
using Infonations;
using Infonations.Models.Common;
using Infonations.Models.Country;

namespace Infonations_WPF.ViewModels
{
    /// <summary>
    /// Holidays Listing (Nager.Date) ViewModel
    /// </summary>
    public class HolidaysListingViewModel : Screen
    {
        private CountryModel _selectedCountry;
        private List<HolidayModel> _holidays;

        /// <summary>
        /// Currently Selected Country (sent by the Country Details ViewModel)
        /// </summary>
        public CountryModel SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                NotifyOfPropertyChange(() => SelectedCountry);
            }
        }

        /// <summary>
        /// List of Holidays
        /// </summary>
        public List<HolidayModel> Holidays
        {
            get { return _holidays; }
            set
            {
                _holidays = value;
                NotifyOfPropertyChange(() => Holidays);
            }
        }

        /// <summary>
        /// ViewModel constructor. Sets the SelectedCountry and asks for the data belonging to this country
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">Currently selected country</param>
        public HolidaysListingViewModel(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry)
        {
            this.SelectedCountry = SelectedCountry;

            LoadData(progress, cToken);
        }

        /// <summary>
        /// Loads the matching data from the API/Database
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        private async void LoadData(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            InfonationsData dataAccess = new InfonationsData(@".\Data");

            await dataAccess.GetNagerDateAPIData(progress, cToken, this.SelectedCountry);

            if (this.SelectedCountry.Holidays != null && this.SelectedCountry.Holidays.Count > 0)
            {
                this.Holidays = this.SelectedCountry.Holidays;
            }
            else
            {
                this.Holidays = new List<HolidayModel>();
                this.Holidays.Add(
                    new HolidayModel()
                    {
                        Name = "N/A",
                        LocalName = "N/A",
                        Date = DateTime.Now,
                        IsFixed = false,
                        IsGlobal = false,
                        LaunchYear = DateTime.Now.Year.ToString()
                    }
                );
            }
        }
    }
}
