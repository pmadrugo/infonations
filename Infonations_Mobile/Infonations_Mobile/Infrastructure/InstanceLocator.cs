﻿using Infonations_Mobile.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infonations_Mobile.Infrastructure
{
    public class InstanceLocator
    {
        public MainViewModel Main { get; set; }

        public InstanceLocator()
        {
            this.Main = new MainViewModel();
        }
    }
}
