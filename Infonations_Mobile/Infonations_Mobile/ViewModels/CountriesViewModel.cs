﻿using Infonations;
using Infonations.Models.Common;
using Infonations.Models.Country;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Xamarin.Forms;

namespace Infonations_Mobile.ViewModels
{
    public class CountriesViewModel : BaseViewModel
    {
        private InfonationsData _infoNations;

        public InfonationsData InfoNations
        {
            get
            {
                if (_infoNations == null)
                    _infoNations = new InfonationsData($"{Environment.GetFolderPath(Environment.SpecialFolder.Personal)}/Data");
                return _infoNations;
            }
        }

        public Progress<ResponseModel> ProgressIndicator { get; set; }

        public CancellationTokenSource CTS { get; set; }

        private ObservableCollection<CountryModel> _countries;
        public ObservableCollection<CountryModel> Countries
        {
            get => _countries;
            set => SetValue(ref _countries, value);
        }



        public CountriesViewModel()
        {
            this.ProgressIndicator = new Progress<ResponseModel>(OnProgressChanged);
            this.CTS = new CancellationTokenSource();

            LoadInfonations();
        }

        public async void LoadInfonations()
        {
            await InfoNations.LoadData(this.ProgressIndicator, this.CTS.Token);

            this.Countries = new ObservableCollection<CountryModel>(InfoNations.Countries.OrderBy(x => x.Name));
        }

        public void OnProgressChanged(ResponseModel incomingReport)
        {

        }
    }
}