﻿using Infonations_Mobile.ViewModels.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Infonations_Mobile.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public ObservableCollection<MenuItemViewModel> Menus { get; set; }

        private static MainViewModel _instance;

        //Menus
        private void LoadMenus()
        {
            //this.Menus = 
            //    new ObservableCollection<MenuItemViewModel>(
            //        new MenuItemViewModel()
            //        {
            //            Icon = "",
            //            Page = "",
            //            Title = ""
            //        }
            //    );

            
        }

        public MainViewModel()
        {
            _instance = this;
            Countries = new CountriesViewModel();
            // LoadMenus();
        }

        public static MainViewModel GetInstance()
        {
            if (_instance == null)
                _instance = new MainViewModel();
            return _instance;
        }


        public CountriesViewModel Countries { get; set; }

    }
}
