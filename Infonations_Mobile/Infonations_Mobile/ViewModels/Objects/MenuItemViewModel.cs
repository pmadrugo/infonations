﻿namespace Infonations_Mobile.ViewModels.Objects
{
    public class MenuItemViewModel
    {
        public string Icon { get; set; }

        public string Page { get; set; }

        public string Title { get; set; }
    }
}