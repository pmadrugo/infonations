﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Models.Common;
using Infonations.Models.Country;
using Infonations.Services.Database;
using Infonations.Services.Models.API.Hellosalut;
using Infonations.Services.Models.API.Nager.Date;
using Infonations.Services.Models.API.Rates;
using Infonations.Services.Models.API.RestCountries;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace Infonations.Services
{
    /// <summary>
    /// Service responsible for handling database related methods
    /// </summary>
    public class DatabaseService
    {
        public string DatabasePath { get; set; }

        public DatabaseService(string databasePath)
        {
            this.DatabasePath = databasePath;

            if (!Directory.Exists(DatabasePath))
            {
                Directory.CreateDirectory(DatabasePath);
            }
        }

        /// <summary>
        /// Saves all Rates from the indicated RatesModel list into the EF6-Managed database
        /// </summary>
        /// <param name="rates">List of RatesModel (API raw data)</param>
        /// <returns>Response Model indicating if successful</returns>
        internal async Task<ResponseModel> RatesSaveAllToDB(List<RatesModel> rates)
        {
            using (RatesAPIContext db = new RatesAPIContext(DatabasePath))
            {
                try
                {
                    var result = await db.ClearDatabase();
                    if (!result.IsSuccess)
                    {
                        throw (Exception)result.Result;
                    }

                    db.Rates.AddRange(rates);

                    await db.SaveChangesAsync();

                    return new ResponseModel()
                    {
                        IsSuccess = true,
                        Message = $"All Rates succesfully saved in database"
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"{ex.Message}",
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Loads all RatesModel currently inserted into the EF6-Managed database
        /// </summary>
        /// <returns>Response Model containing a list of RatesModel (Raw API Data)</returns>
        internal async Task<ResponseModel> RatesLoadFromDB()
        {
            using (RatesAPIContext db = new RatesAPIContext(DatabasePath))
            {
                try
                {
                    List<RatesModel> rates = new List<RatesModel>();

                    await Task.Run(
                        () =>
                        {
                            rates = db.Rates.ToList();
                        }
                    );

                    return new ResponseModel()
                    {
                        IsSuccess = true,
                        Message = $"All rates successfully loaded from database",
                        Result = rates
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"{ex.Message}",
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Saves all the Restcountries API data into the EF6-Managed database
        /// </summary>
        /// <param name="restCountries">List of RestCountriesModel (Raw API Data)</param>
        /// <returns>Response Model indicating if successful</returns>
        internal async Task<ResponseModel> RestCountriesSaveAllToDB(List<RestCountriesModel> restCountries)
        {
            using (RestCountriesEUContext db = new RestCountriesEUContext(DatabasePath))
            {
                try
                {
                    await db.ClearDatabase();
                    
                    db.RestCountries.AddRange(restCountries);

                    await db.SaveChangesAsync();

                    return new ResponseModel()
                    {
                        IsSuccess = true,
                        Message = $"RestCountries successfully saved to database"
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"{ex.Message}",
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Loads all the RestCountriesModel objects from the EF6-Managed Database
        /// </summary>
        /// <returns>Response Model containing a list of RestCountriesModel</returns>
        internal async Task<ResponseModel> RestCountriesLoadAllFromDB()
        {
            using (RestCountriesEUContext db = new RestCountriesEUContext(DatabasePath))
            {
                try
                {
                    List<RestCountriesModel> restCountries = new List<RestCountriesModel>();

                    await Task.Run(
                        () =>
                        {
                            db.RestCountriesRegionalBlocsOtherAcronyms.Load();
                            db.RestCountriesRegionalBlocsOtherNames.Load();

                            restCountries = db.RestCountries
                                .Include("latlng")
                                .Include("currencies")
                                .Include("languages")
                                .Include("topLevelDomain")
                                .Include("callingCodes")
                                .Include("altSpellings")
                                .Include("timezones")
                                .Include("borders")
                                .Include("translations")
                                .Include("regionalBlocs")
                            .ToList();
                        }
                    );

                    return new ResponseModel()
                    {
                        IsSuccess = true,
                        Message = $"Sucessfully loaded RestCountries from Database",
                        Result = restCountries
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"{ex.Message}",
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Updates the HelloSalut DB, if values are missing, they get added, if not, they're updated
        /// </summary>
        /// <param name="helloSalutEntry">The current HelloSalut entry being processed</param>
        /// <returns>Response Model indicating if successful</returns>
        internal async Task<ResponseModel> HelloSalutUpdateDB(HellosalutModel helloSalutEntry )
        {
            using (HelloSalutContext db = new HelloSalutContext(DatabasePath))
            {
                try
                {
                    var result = await db.UpdateDatabase(helloSalutEntry);

                    return result;
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = ex.Message,
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Get a country's HelloSalut corresponding value from the database
        /// </summary>
        /// <param name="SelectedCountry">Country to which one wants to acquire "Hello"</param>
        /// <returns>ResponseModel containing the "hello" if succesful</returns>
        internal ResponseModel HelloSalutGetFromDB(CountryModel SelectedCountry)
        {
            using (HelloSalutContext db = new HelloSalutContext(DatabasePath))
            {
                try
                {
                    var result = db.HelloSalut.Where((helloSalut) => helloSalut.code == SelectedCountry.Alpha2Code).SingleOrDefault();
                    result.hello = WebUtility.HtmlDecode(result.hello);

                    if (result != null)
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = true,
                            Result = result
                        };
                    }
                    else
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = false,
                            Message = $"No record found"
                        };
                    }
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = ex.Message,
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Updates the Nager.Date DB, if values are missing, they get added, if not, they're updated
        /// </summary>
        /// <param name="NagerDateEntries">List of holidays belonging to a country</param>
        /// <param name="SelectedCountry">The country to which the holidays belong</param>
        /// <returns>Response Model indicating if successful</returns>
        internal async Task<ResponseModel> NagerDateUpdateDB(List<NagerDateModel> NagerDateEntries, CountryModel SelectedCountry)
        {
            using (NagerDateContext db = new NagerDateContext(DatabasePath))
            {
                try
                {
                    var result = await db.UpdateDatabase(NagerDateEntries, SelectedCountry);

                    return result;
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = ex.Message,
                        Result = ex
                    };
                }
            }
        }

        /// <summary>
        /// Get Nager.Date items from the EF6-Managed database
        /// </summary>
        /// <param name="SelectedCountry">The country to which one wants to get the holidays</param>
        /// <returns>Response model indicating whether successful (with results) or not</returns>
        internal ResponseModel NagerDateGetFromDB(CountryModel SelectedCountry)
        {
            using (NagerDateContext db = new NagerDateContext(DatabasePath))
            {
                try
                {
                    var result = db.NagerDate.ToList().Where(
                        (nagerDate) =>
                        (
                            (nagerDate.countryCode == SelectedCountry.Alpha2Code) && (DateTime.Parse(nagerDate.date).Year == DateTime.Now.Year)
                        )
                    ).ToList();

                    return new ResponseModel()
                    {
                        IsSuccess = true,
                        Result = result
                    };
                }
                catch (Exception ex)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = ex.Message,
                        Result = ex
                    };
                }
            }
        }
    }
}
