﻿namespace Infonations.Services.Models.API.Nager.Date
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for the Nager.Date API result
    /// </summary>
    public class NagerDateModel
    {

        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [Key] [JsonIgnore]
        public int ID { get; set; }

        /// <summary>
        /// The holiday's date
        /// </summary>
        [JsonProperty(PropertyName = "date")]
        public string date { get; set; }

        /// <summary>
        /// The holiday's name in its local language
        /// </summary>
        [JsonProperty(PropertyName = "localName")]
        public string localName { get; set; }

        /// <summary>
        /// The holiday's name in english
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// The alpha 2 code of the country to which the holiday belongs
        /// </summary>
        [JsonProperty(PropertyName = "countryCode")]
        public string countryCode { get; set; }

        /// <summary>
        /// Is the holiday fixed throughout the years?
        /// </summary>
        [JsonProperty(PropertyName = "fixed")]
        public bool @fixed { get; set; }

        /// <summary>
        /// Is it a county official holiday?
        /// </summary>
        [JsonProperty(PropertyName = "countyOfficialHoliday")]
        public bool countyOfficialHoliday { get; set; }

        /// <summary>
        /// Is it a county administrative holiday?
        /// </summary>
        [JsonProperty(PropertyName = "countyAdministrationHoliday")]
        public bool countyAdministrationHoliday { get; set; }

        /// <summary>
        /// Property indicating if the holiday is celebrated outside this country
        /// </summary>
        [JsonProperty(PropertyName = "global")]
        public bool global { get; set; }

        /// <summary>
        /// Counties in the country that celebrate this holiday
        /// </summary>
        [JsonProperty(PropertyName = "counties")]
        public List<string> counties { get; set; }

        /// <summary>
        /// Years in which the holiday started being celebrated
        /// </summary>
        [JsonProperty(PropertyName = "launchYear")]
        public int? launchYear { get; set; }
    }
}
