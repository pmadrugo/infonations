﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\TimeZone
    /// </summary>
    public class RestCountriesTimeZoneModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Timezone Designation
        /// </summary>
        public string TimeZone { get; set; }
    }
}