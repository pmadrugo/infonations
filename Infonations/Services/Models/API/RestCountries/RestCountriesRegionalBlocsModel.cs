﻿namespace Infonations.Services.Models.API.RestCountries
{
    using Infonations.Services.Helpers;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// RestCountries\RegionalBlocs
    /// </summary>
    public class RestCountriesRegionalBlocsModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// The regional bloc's acronym
        /// </summary>
        [JsonProperty(PropertyName = "acronym")]
        public string acronym { get; set; }

        /// <summary>
        /// The regional bloc's name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// The regional bloc's other acronyms
        /// </summary>
        [JsonProperty(PropertyName = "otherAcronyms", ItemConverterType = typeof(RestCountriesRegionalBlocsOtherAcronymsDeserializer))]
        public List<RestCountriesRegionalBlocsOtherAcronymsModel> otherAcronyms { get; set; }

        /// <summary>
        /// The regional bloc's other names
        /// </summary>
        [JsonProperty(PropertyName = "otherNames", ItemConverterType = typeof(RestCountriesRegionalBlocsOtherNamesDeserializer))]
        public List<RestCountriesRegionalBlocsOtherNamesModel> otherNames { get; set; }
    }
}
