﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\Coords
    /// </summary>
    public class RestCountriesCoordsModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Country's latitude
        /// </summary>
        public double latitude { get; set; }

        /// <summary>
        /// Country's longitude
        /// </summary>
        public double longitude { get; set; }
    }
}