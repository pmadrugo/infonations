﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\Language
    /// </summary>
    public class RestCountriesLanguageModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// The language ISO 639-1 code
        /// </summary>
        [JsonProperty(PropertyName = "iso639_1")]
        public string iso639_1 { get; set; }

        /// <summary>
        /// The language ISO 639-2 code
        /// </summary>
        [JsonProperty(PropertyName = "iso639_2")]
        public string iso639_2 { get; set; }

        /// <summary>
        /// The language name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// The language name in its own language
        /// </summary>
        [JsonProperty(PropertyName = "nativeName")]
        public string nativeName { get; set; }
    }
}
