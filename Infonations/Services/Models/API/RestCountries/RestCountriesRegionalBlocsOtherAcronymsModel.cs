﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Infonations.Services.Models.API.RestCountries
{
    /// <summary>
    /// RestCountries\RegionalBlocs\OtherAcronyms
    /// </summary>
    public class RestCountriesRegionalBlocsOtherAcronymsModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [Key] [JsonIgnore]
        public int ID { get; set; }

        /// <summary>
        /// Regional Bloc other acronym
        /// </summary>
        public string OtherAcronym { get; set; }
    }
}
