﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\Currencies
    /// </summary>
    public class RestCountriesCurrencyModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Currency Code
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        /// <summary>
        /// Currency name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// Currency symbol
        /// </summary>
        [JsonProperty(PropertyName = "symbol")]
        public string symbol { get; set; }

        /// <summary>
        /// Country ID to which the currency belongs
        /// </summary>
        public int RestCountryID { get; set; }

        /// <summary>
        /// Virtual property pointing to the country
        /// </summary>
        public virtual RestCountriesModel RestCountry { get; set; }
    }
}
