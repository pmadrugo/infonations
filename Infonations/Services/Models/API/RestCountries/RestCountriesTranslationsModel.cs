﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\Translations
    /// </summary>
    public class RestCountriesTranslationsModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// The country's name in German
        /// </summary>
        [JsonProperty(PropertyName = "de")]
        public string de { get; set; } // German

        /// <summary>
        /// The country's name in Spanish
        /// </summary>
        [JsonProperty(PropertyName = "es")]
        public string es { get; set; } // Spanish

        /// <summary>
        /// The country's name in French
        /// </summary>
        [JsonProperty(PropertyName = "fr")]
        public string fr { get; set; } // French

        /// <summary>
        /// The country's name in Japanese
        /// </summary>
        [JsonProperty(PropertyName = "ja")]
        public string ja { get; set; } // Japanese

        /// <summary>
        /// The country's name in Italian
        /// </summary>
        [JsonProperty(PropertyName = "it")]
        public string it { get; set; } // Italian

        /// <summary>
        /// The country's name in Brazillian
        /// </summary>
        [JsonProperty(PropertyName = "br")]
        public string br { get; set; } // Brazillian

        /// <summary>
        /// The country's name in Portuguese
        /// </summary>
        [JsonProperty(PropertyName = "pt")]
        public string pt { get; set; } // Portuguese

        /// <summary>
        /// The country's name in Dutch
        /// </summary>
        [JsonProperty(PropertyName = "nl")]
        public string nl { get; set; } // Dutch (Netherlands)

        /// <summary>
        /// The country's name in Magyar
        /// </summary>
        [JsonProperty(PropertyName = "hr")]
        public string hr { get; set; } // Magyar (Hungary)


        /// <summary>
        /// The country's name in Farsi
        /// </summary>
        [JsonProperty(PropertyName = "fa")]
        public string fa { get; set; } // Farsi (Iran)
    }
}
