﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Infonations.Services.Helpers;
    using Newtonsoft.Json;

    public class RestCountriesModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Country name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string name { get; set; }

        /// <summary>
        /// Country's top level domain(s)
        /// </summary>
        [JsonProperty(PropertyName = "topLevelDomain", ItemConverterType = typeof(RestCountriesTopLevelDomainDeserializer), ObjectCreationHandling = ObjectCreationHandling.Reuse)]
        [ForeignKey("RestCountryID")]
        public virtual List<RestCountriesTopLevelDomainModel> topLevelDomain { get; set; }

        /// <summary>
        /// Country's Alpha 2 Code
        /// </summary>
        [JsonProperty(PropertyName = "alpha2Code")]
        public string alpha2Code { get; set; }

        /// <summary>
        /// Country's Alpha 3 Code
        /// </summary>
        [JsonProperty(PropertyName = "alpha3Code")]
        public string alpha3Code { get; set; }

        /// <summary>
        /// Country's calling codes
        /// </summary>
        [JsonProperty(PropertyName = "callingCodes", ItemConverterType = typeof(RestCountriesCallingCodeDeserializer))]
        public List<RestCountriesCallingCodeModel> callingCodes { get; set; }

        /// <summary>
        /// Country's capital
        /// </summary>
        [JsonProperty(PropertyName = "capital")]
        public string capital { get; set; }

        /// <summary>
        /// Country's alternative spellings
        /// </summary>
        [JsonProperty(PropertyName = "altSpellings", ItemConverterType = typeof(RestCountriesAltSpellingsDeserializer))]
        public List<RestCountriesAltSpellingsModel> altSpellings { get; set; }

        /// <summary>
        /// Country's region
        /// </summary>
        [JsonProperty(PropertyName = "region")]
        public string region { get; set; }

        /// <summary>
        /// Country's subregion
        /// </summary>
        [JsonProperty(PropertyName = "subregion")]
        public string subregion { get; set; }

        /// <summary>
        /// Country's population
        /// </summary>
        [JsonProperty(PropertyName = "population")]
        public long? population { get; set; }

        /// <summary>
        /// Country's coordinates (latitude/longitude)
        /// </summary>
        [JsonProperty(PropertyName = "latlng")]
        [JsonConverter(typeof(RestCountriesCoordsDeserializer))]
        public RestCountriesCoordsModel latlng { get; set; }

        /// <summary>
        /// Country's demonym (name of its people)
        /// </summary>
        [JsonProperty(PropertyName = "demonym")]
        public string demonym { get; set; }

        /// <summary>
        /// Country's area/size
        /// </summary>
        [JsonProperty(PropertyName = "area")]
        public double? area { get; set; }

        /// <summary>
        /// Country's GINI value
        /// </summary>
        [JsonProperty(PropertyName = "gini")]
        public double? gini { get; set; } // GINI Index

        /// <summary>
        /// Country's timezones
        /// </summary>
        [JsonProperty(PropertyName = "timezones", ItemConverterType = typeof(RestCountriesTimeZoneDeserializer))]
        public List<RestCountriesTimeZoneModel> timezones { get; set; } // TimeZones

        /// <summary>
        /// Country's bordering countries (in Alpha 3 Code)
        /// </summary>
        [JsonProperty(PropertyName = "borders", ItemConverterType= typeof(RestCountriesBorderDeserializer))]
        public List<RestCountriesBorderModel> borders { get; set; } // Countries that border this one (Alphacode3)

        /// <summary>
        /// Country's name in its own native language
        /// </summary>
        [JsonProperty(PropertyName = "nativeName")]
        public string nativeName { get; set; } // Native Name

        /// <summary>
        /// Country's numeric code
        /// </summary>
        [JsonProperty(PropertyName = "numericCode")]
        public string numericCode { get; set; }

        /// <summary>
        /// Country's currencies
        /// </summary>
        [JsonProperty(PropertyName = "currencies")]
        [ForeignKey("RestCountryID")]
        public List<RestCountriesCurrencyModel> currencies { get; set; } // List of currencies

        /// <summary>
        /// Country's languages
        /// </summary>
        [JsonProperty(PropertyName = "languages")]
        public List<RestCountriesLanguageModel> languages { get; set; }

        /// <summary>
        /// Country name in other languages
        /// </summary>
        [JsonProperty(PropertyName = "translations")]
        public RestCountriesTranslationsModel translations { get; set; }

        /// <summary>
        /// Flag Path (Online) ; SVG Format
        /// </summary>
        [JsonProperty(PropertyName = "flag")]
        public string flag { get; set; }

        /// <summary>
        /// List of regional blocs the country belongs to
        /// </summary>
        [JsonProperty(PropertyName = "regionalBlocs")]
        public List<RestCountriesRegionalBlocsModel> regionalBlocs { get; set; }

        /// <summary>
        /// Code - International Olympics Committee
        /// </summary>
        [JsonProperty(PropertyName = "cioc")]
        public string CIOC { get; set; } // Code International Olympic Comitee
    }
}
