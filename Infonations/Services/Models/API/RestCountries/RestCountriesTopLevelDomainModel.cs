﻿namespace Infonations.Services.Models.API.RestCountries
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Newtonsoft.Json;

    /// <summary>
    /// RestCountries\TopLevelDomain
    /// </summary>
    public class RestCountriesTopLevelDomainModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [JsonIgnore] [Key]
        public int ID { get; set; }

        /// <summary>
        /// Country's top level domain (Internet suffix)
        /// </summary>
        public string TopLevelDomain { get; set; }

        /// <summary>
        /// The country's ID that this top level domain belongs to
        /// </summary>
        public int RestCountryID { get; set; }
        /// <summary>
        /// The rest country to which this ID corresponds
        /// </summary>
        public virtual RestCountriesModel RestCountry { get; set; }
    }
}
