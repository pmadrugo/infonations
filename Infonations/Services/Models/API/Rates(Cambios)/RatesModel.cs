﻿namespace Infonations.Services.Models.API.Rates
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Model for the Rates (Câmbios) API
    /// </summary>
    public class RatesModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [Key]
        public int RateId { get; set; }

        /// <summary>
        /// The currency code to which the rate belongs to
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// The rate of conversion against an Euro
        /// </summary>
        public double TaxRate { get; set; }

        /// <summary>
        /// The currency name
        /// </summary>
        public string Name { get; set; }
    }
}
