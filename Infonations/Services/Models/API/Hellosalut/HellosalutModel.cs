﻿namespace Infonations.Services.Models.API.Hellosalut
{
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;

    /// <summary>
    /// The model for the HelloSalut API
    /// </summary>
    public class HellosalutModel
    {
        /// <summary>
        /// ID used internally by the EF6 -- DO NOT CHANGE VALUE
        /// </summary>
        [Key] [JsonIgnore]
        public int ID { get; set; }

        /// <summary>
        /// The Alpha 2 Code to which the Hello belongs to
        /// </summary>
        [JsonProperty(PropertyName = "code")]
        public string code { get; set; }

        /// <summary>
        /// Property containing the Hello text
        /// </summary>
        [JsonProperty(PropertyName = "hello")]
        public string hello { get; set; }
    }
}
