﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Infonations.Models.Common;

namespace Infonations.Services
{
    /// <summary>
    /// Service responsible for checking if there's internet connection
    /// </summary>
    public static class NetworkService
    {
        /// <summary>
        /// Checks if there's a currently working internet connection 
        /// </summary>
        /// <returns>Response Model containing if successful or not</returns>
        public static ResponseModel CheckConnection()
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = true
                        };
                    }
                }
                catch (Exception)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"Internet connection not found."
                    };
                }
            }
        }

        /// <summary>
        /// Checks if there indicated URL is currently in service
        /// </summary>
        /// <param name="URL">URL to check if there's connection</param>
        /// <returns>Response Model containing if successful or not</returns>
        public static ResponseModel CheckConnectionTo(string URL)
        {
            using (WebClient client = new WebClient())
            {
                try
                {
                    using (client.OpenRead(URL))
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = true
                        };
                    }
                }
                catch (Exception)
                {
                    return new ResponseModel()
                    {
                        IsSuccess = false,
                        Message = $"Internet connection not found."
                    };
                }
            }
        }
    }
}
