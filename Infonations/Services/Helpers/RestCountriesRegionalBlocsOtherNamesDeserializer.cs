﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for a regional bloc's other names
    /// </summary>
    public class RestCountriesRegionalBlocsOtherNamesDeserializer : JsonConverter<RestCountriesRegionalBlocsOtherNamesModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesRegionalBlocsOtherNamesModel ReadJson(JsonReader reader, Type objectType, RestCountriesRegionalBlocsOtherNamesModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string otherName = (string)reader.Value;

            return new RestCountriesRegionalBlocsOtherNamesModel()
            {
                OtherName = otherName
            };
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesRegionalBlocsOtherNamesModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.OtherName);
        }
    }
}
