﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for Borders
    /// </summary>
    public class RestCountriesBorderDeserializer : JsonConverter<RestCountriesBorderModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesBorderModel ReadJson(JsonReader reader, Type objectType, RestCountriesBorderModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string border = (string)reader.Value;

            return new RestCountriesBorderModel()
            {
                Border = border
            };
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesBorderModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.Border);
        }
    }
}
