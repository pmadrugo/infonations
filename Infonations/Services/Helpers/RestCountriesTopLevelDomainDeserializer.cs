﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for a Top Level Domain
    /// </summary>
    public class RestCountriesTopLevelDomainDeserializer : JsonConverter<RestCountriesTopLevelDomainModel>
    {
        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesTopLevelDomainModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.TopLevelDomain);
        }

        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesTopLevelDomainModel ReadJson(JsonReader reader, Type objectType, RestCountriesTopLevelDomainModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string topLevelDomain = (string)reader.Value;

            return new RestCountriesTopLevelDomainModel()
            {
                TopLevelDomain = topLevelDomain
            };
        }
    }
}
