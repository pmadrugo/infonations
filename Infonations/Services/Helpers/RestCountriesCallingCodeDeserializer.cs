﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for Calling Codes
    /// </summary>
    public class RestCountriesCallingCodeDeserializer : JsonConverter<RestCountriesCallingCodeModel>
    {
        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesCallingCodeModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.CallingCode);
        }

        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesCallingCodeModel ReadJson(JsonReader reader, Type objectType, RestCountriesCallingCodeModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string callingCode = (string)reader.Value;

            return new RestCountriesCallingCodeModel()
            {
                CallingCode = callingCode
            };
        }
    }
}
