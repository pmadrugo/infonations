﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for Coordinates
    /// </summary>
    public class RestCountriesCoordsDeserializer : JsonConverter<RestCountriesCoordsModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesCoordsModel ReadJson(JsonReader reader, Type objectType, RestCountriesCoordsModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            RestCountriesCoordsModel newCoords = new RestCountriesCoordsModel();
            
            int pos = 0;

            while (reader.Read())
            {
                if (reader.TokenType == JsonToken.EndArray)
                {
                    break;
                }

                switch (pos)
                {
                    case 0: {
                        if (reader.Value == null)
                        {
                            newCoords.latitude = 0;
                        }
                        else
                        {
                            newCoords.latitude = (double)reader.Value;
                        }
                    } break;
                    case 1: {
                        if (reader.Value == null)
                        {
                            newCoords.longitude = 0;
                        }
                        else
                        {
                            newCoords.longitude = (double)reader.Value;
                        }
                    } break;
                }
                pos++;
            };

            return newCoords;
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesCoordsModel value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
