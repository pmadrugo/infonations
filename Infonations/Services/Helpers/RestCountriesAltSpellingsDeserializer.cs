﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for Alt Spellings
    /// </summary>
    public class RestCountriesAltSpellingsDeserializer : JsonConverter<RestCountriesAltSpellingsModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesAltSpellingsModel ReadJson(JsonReader reader, Type objectType, RestCountriesAltSpellingsModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string altSpelling = (string)reader.Value;

            return new RestCountriesAltSpellingsModel()
            {
                AltSpelling = altSpelling
            };
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesAltSpellingsModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.AltSpelling);
        }
    }
}
