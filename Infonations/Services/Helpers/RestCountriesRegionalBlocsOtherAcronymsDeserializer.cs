﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for a regional bloc's other acronyms
    /// </summary>
    public class RestCountriesRegionalBlocsOtherAcronymsDeserializer : JsonConverter<RestCountriesRegionalBlocsOtherAcronymsModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesRegionalBlocsOtherAcronymsModel ReadJson(JsonReader reader, Type objectType, RestCountriesRegionalBlocsOtherAcronymsModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string otherAcronym = (string)reader.Value;

            return new RestCountriesRegionalBlocsOtherAcronymsModel()
            {
                OtherAcronym = otherAcronym
            };
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesRegionalBlocsOtherAcronymsModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.OtherAcronym);
        }
    }
}
