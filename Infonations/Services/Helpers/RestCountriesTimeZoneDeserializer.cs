﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services.Helpers
{
    /// <summary>
    /// Deserializer helper for Timezone
    /// </summary>
    public class RestCountriesTimeZoneDeserializer : JsonConverter<RestCountriesTimeZoneModel>
    {
        /// <summary>
        /// From JSON
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="hasExistingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override RestCountriesTimeZoneModel ReadJson(JsonReader reader, Type objectType, RestCountriesTimeZoneModel existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string timezone = (string)reader.Value;

            return new RestCountriesTimeZoneModel()
            {
                TimeZone = timezone
            };
        }

        /// <summary>
        /// To JSON
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, RestCountriesTimeZoneModel value, JsonSerializer serializer)
        {
            writer.WriteValue(value.TimeZone);
        }
    }
}
