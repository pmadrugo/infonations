﻿namespace Infonations.Services.Database
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;
    using Infonations.Models.Common;
    using Infonations.Services.Models.API.RestCountries;

    /// <summary>
    /// Entity Framework Database Context used for saving the RestCountries API
    /// </summary>
    internal class RestCountriesEUContext : DbContext
    {
        public string DatabasePath { get; set; }

        internal RestCountriesEUContext(string filePath) : base()
        {
            this.DatabasePath = filePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite($"Data Source={DatabasePath}");
        }

        #region Models/Tables

        /// <summary>
        /// Table holding the base country model
        /// </summary>
        public DbSet<RestCountriesModel>  RestCountries { get; set; }

        /// <summary>
        /// Table holding the currency models
        /// </summary>
        public DbSet<RestCountriesCurrencyModel> RestCountriesCurrency { get; set; }

        /// <summary>
        /// Table holding the language models
        /// </summary>
        public DbSet<RestCountriesLanguageModel> RestCountriesLanguage { get; set; }

        /// <summary>
        /// Table holding the regional blocs models
        /// </summary>
        public DbSet<RestCountriesRegionalBlocsModel> RestCountriesRegionalBlocs { get; set; }

        /// <summary>
        /// Table holding the translations models
        /// </summary>
        public DbSet<RestCountriesTranslationsModel> RestCountriesTranslations { get; set; }

        /// <summary>
        /// Table holding the top level domains
        /// </summary>
        public DbSet<RestCountriesTopLevelDomainModel> RestCountriesTopLevelDomains { get; set; }

        /// <summary>
        /// Table holding the calling codes
        /// </summary>
        public DbSet<RestCountriesCallingCodeModel> RestCountriesCallingCodes { get; set; }

        /// <summary>
        /// Table holding the alternative spellings
        /// </summary>
        public DbSet<RestCountriesAltSpellingsModel> RestCountriesAltSpellings { get; set; }

        /// <summary>
        /// Table holding the border informations
        /// </summary>
        public DbSet<RestCountriesBorderModel> RestCountriesBorders { get; set; }

        /// <summary>
        /// Table holding the country's coordinates
        /// </summary>
        public DbSet<RestCountriesCoordsModel> RestCountriesCoords { get; set; }

        /// <summary>
        /// Table holding the timezones
        /// </summary>
        public DbSet<RestCountriesTimeZoneModel> RestCountriesTimeZones { get; set; }

        /// <summary>
        /// Table holding the regional bloc's other acronyms
        /// </summary>
        public DbSet<RestCountriesRegionalBlocsOtherAcronymsModel> RestCountriesRegionalBlocsOtherAcronyms { get; set; }

        /// <summary>
        /// Table holding the regional bloc's other names
        /// </summary>
        public DbSet<RestCountriesRegionalBlocsOtherNamesModel> RestCountriesRegionalBlocsOtherNames { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Clears the RestCountries API Database
        /// </summary>
        /// <returns></returns>
        internal async Task<ResponseModel> ClearDatabase()
        {
            try
            {
                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesCurrencyModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesLanguageModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesRegionalBlocsModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesTranslationsModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesTopLevelDomainModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesCallingCodeModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesAltSpellingsModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesBorderModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesCoordsModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesTimeZoneModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesRegionalBlocsOtherAcronymsModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RestCountriesRegionalBlocsOtherNamesModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"VACUUM"
                );

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Database cleared succesfully"
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = $"{ex.Message}",
                    Result = ex
                };
            }
        }
    }

    #endregion

    
}
