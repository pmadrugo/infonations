﻿namespace Infonations.Services.Database
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Infonations.Models.Common;
    using Infonations.Services.Models.API.Hellosalut;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Entity Framework Database Context used for saving the Hello Salut API
    /// </summary>
    internal class HelloSalutContext : DbContext
    {
        public string DatabasePath { get; set; }

        internal HelloSalutContext(string filePath) : base()
        {
            this.DatabasePath = filePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite($"Data Source={DatabasePath}");
        }

        /// <summary>
        /// Only table
        /// </summary>
        public DbSet<HellosalutModel> HelloSalut { get; set; }

        /// <summary>
        /// Clears the Hellosalut API Database
        /// </summary>
        /// <returns>Response Model</returns>
        internal async Task<ResponseModel> ClearDatabase()
        {
            try
            {
                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM HelloSalutModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"VACUUM"
                );

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Database cleared succesfully"
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = ex
                };
            }
        }

        /// <summary>
        /// Updates the Hello Salut Database
        /// </summary>
        /// <param name="HelloSalutEntry">The entry received from the API call</param>
        /// <returns>Response Model</returns>
        internal async Task<ResponseModel> UpdateDatabase(HellosalutModel HelloSalutEntry)
        {
            try
            {
                var helloSalutResult = this.HelloSalut.Where((helloSalutDB) => helloSalutDB.code == HelloSalutEntry.code).SingleOrDefault();

                if (helloSalutResult != null)
                {
                    helloSalutResult.code = HelloSalutEntry.code;
                    helloSalutResult.hello = HelloSalutEntry.hello;
                }
                else
                {
                    this.HelloSalut.Add(
                        new HellosalutModel()
                        {
                            code = HelloSalutEntry.code,
                            hello = HelloSalutEntry.hello
                        }
                    );
                }

                await this.SaveChangesAsync();

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Hello Salut entry succesfully updated."
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = ex
                };
            }
        }
    }
}
