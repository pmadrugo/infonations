﻿namespace Infonations.Services.Database
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Infonations.Models.Common;
    using Infonations.Models.Country;
    using Infonations.Services.Models.API.Nager.Date;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Entity Framework Database Context used for saving the Nager.Date API
    /// </summary>
    internal class NagerDateContext : DbContext
    {
        public string DatabasePath { get; set; }

        internal NagerDateContext(string filePath) : base()
        {
            this.DatabasePath = filePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite($"Data Source={DatabasePath}");
        }

        /// <summary>
        /// Only table
        /// </summary>
        public DbSet<NagerDateModel> NagerDate { get; set; }

        /// <summary>
        /// Clears the Nager.Date API Database
        /// </summary>
        /// <returns></returns>
        internal async Task<ResponseModel> ClearDatabase()
        {
            try
            {
                await this.Database.ExecuteSqlCommandAsync(
                @"DELETE FROM NagerDateModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"VACUUM"
                );

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Nager Date database updated successfully."
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = ex
                };
            }
        }

        /// <summary>
        /// Updates the Nager.Date database
        /// </summary>
        /// <param name="nagerDateEntries">The holidays listing received by the API call</param>
        /// <param name="SelectedCountry">The current country to which the holidays belong</param>
        /// <returns>Response Model</returns>
        internal async Task<ResponseModel> UpdateDatabase(List<NagerDateModel> nagerDateEntries, CountryModel SelectedCountry)
        {
            try
            {
                var alpha2Code = SelectedCountry.Alpha2Code;

                var currentRecords =  await this.NagerDate.Where((nagerDB) => nagerDB.countryCode == alpha2Code).ToListAsync();

                this.NagerDate.RemoveRange(currentRecords);
                this.NagerDate.AddRange(nagerDateEntries);

                await this.SaveChangesAsync();

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Database updated successfully"
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = ex.Message,
                    Result = ex
                };
            }
        }
    }
}
