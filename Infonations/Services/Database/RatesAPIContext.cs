﻿namespace Infonations.Services.Database
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using System.Threading.Tasks;
    using Infonations.Models.Common;
    using Infonations.Services.Models.API.Rates;

    /// <summary>
    /// Entity Framework Database Context used for saving the Rates (Câmbios) API
    /// </summary>
    internal class RatesAPIContext : DbContext
    {
        public string DatabasePath { get; set; }

        internal RatesAPIContext(string filePath) : base()
        {
            this.DatabasePath = filePath;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite($"Data Source={DatabasePath}");
        }

        #region Models/Tables

        /// <summary>
        /// Only table
        /// </summary>
        public DbSet<RatesModel> Rates { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Clears the Rates (Câmbios) API
        /// </summary>
        /// <returns>Response Model</returns>
        internal async Task<ResponseModel> ClearDatabase()
        {
            try
            {
                await this.Database.ExecuteSqlCommandAsync(
                    @"DELETE FROM RatesModels"
                );

                await this.Database.ExecuteSqlCommandAsync(
                    @"VACUUM"
                );

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Database cleared succesfully"
                };
            }
            catch (Exception ex)
            {
                return new ResponseModel()
                {
                    IsSuccess = false,
                    Message = $"{ex.Message}",
                    Result = ex
                };
            }
        }

        #endregion

    }
}
