﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Infonations.Models.Common;
using Infonations.Models.Country;
using Infonations.Services.Database;
using Infonations.Services.Models.API.Hellosalut;
using Infonations.Services.Models.API.Nager.Date;
using Infonations.Services.Models.API.Rates;
using Infonations.Services.Models.API.RestCountries;
using Newtonsoft.Json;

namespace Infonations.Services
{
    /// <summary>
    /// Service responsible for handling API related methods
    /// </summary>
    public class APIService
    {
        public string DatabasePath { get; set; }

        public APIService(string filePath)
        {
            this.DatabasePath = filePath;
        }

        /// <summary>
        /// Acquires information from the specified API while returning information regarding its loading
        /// </summary>
        /// <param name="APIBaseURL">Base URL</param>
        /// <param name="APIControllerURL">Specific controller url</param>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns>Response Model indicating if successful or not</returns>
        public async Task<ResponseModel> GetJSONAPI(string APIBaseURL, string APIControllerURL, IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            try
            {
                using (HttpClient httpConnection = new HttpClient() { BaseAddress = new Uri(APIBaseURL) })
                {
                    using (HttpResponseMessage response = await httpConnection.GetAsync(APIControllerURL))
                    {
                        string result = await response.Content.ReadAsStringAsync();

                        if (!response.IsSuccessStatusCode)
                        {
                            return new ResponseModel()
                            {
                                IsSuccess = false,
                                Message = result
                            };
                        }

                        return new ResponseModel()
                        {
                            IsSuccess = true,
                            Result = result
                        };
                    }
                }
            }
            catch (Exception)
            {
                return new ResponseModel()
                {
                    IsSuccess = false
                };
            }
        }


        /// <summary>
        /// Get data from the RestCountries API (Returns list of RestCountriesModels)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns>Response Model indicating if successful (with results) or not</returns>
        internal async Task<ResponseModel> GetRestCountriesData(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            string baseURL = $"https://restcountries.eu";
            string controllerURL = $"/rest/v2/all";
            ResponseModel result = new ResponseModel();

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            progress.Report(
                    new ResponseModel()
                    {
                        Message = $"RestCountriesEU - Loading RestCountries API data..."
                    }
                );

            if (NetworkService.CheckConnectionTo(baseURL + controllerURL).IsSuccess)
            {
                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"RestCountriesEU - Connection successful... Downloading."
                    }
                );

                // If connection is made
                var APIresult = await GetJSONAPI(baseURL, controllerURL, progress, cToken);
                try
                {

                    result.Result = JsonConvert.DeserializeObject<List<RestCountriesModel>>((string)APIresult.Result);
                    result.IsSuccess = true;

                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                

                stopwatch.Stop();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"RestCountriesEU - Data Download finished... {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                    }
                );

                await GetRestCountriesFlag(progress, cToken, (List<RestCountriesModel>)result.Result).ConfigureAwait(false);
            }
            else
            {
                result.IsSuccess = false;
            }

            return result;
        }

        /// <summary>
        /// Fetches the flags from the matching RestCountries objects (Internal use)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="restCountryData">List of RestCountriesModels (RestCountries raw API data)</param>
        /// <returns></returns>
        private async Task GetRestCountriesFlag(IProgress<ResponseModel> progress, CancellationToken cToken, List<RestCountriesModel> restCountryData)
        {
            try
            {
                progress.Report(
                       new ResponseModel()
                       {
                           Message = $"RestCountriesEU - Acquiring flags..."
                       }
                   );

                var stopwatch = System.Diagnostics.Stopwatch.StartNew();
                var flagsDirectory = $"{this.DatabasePath}/Flags/";
                Directory.CreateDirectory($@"{flagsDirectory}");

                using (HttpClient httpClient = new HttpClient())
                {
                    foreach (RestCountriesModel country in restCountryData)
                    {
                        var filePath = country.flag.Substring(country.flag.LastIndexOf('/') + 1);

                        if (!(new FileInfo($@"{flagsDirectory}/{filePath}").Exists))
                        {
                            var getFile = await httpClient.GetAsync(country.flag);
                            using (var memStream = getFile.Content.ReadAsStreamAsync().Result)
                            {
                                using (var fileStream = File.Create($@"{flagsDirectory}/{filePath}"))
                                {
                                    memStream.CopyTo(fileStream);
                                }
                            }
                        }

                    }
                }

                stopwatch.Stop();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"RestCountriesEU - Flags acquired. Took {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                    }
                );

            }
            catch(Exception ex)
            {

            }
        }

        /// <summary>
        /// Gets data from the Rates (Câmbios) API
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns>Response Model indicating if successful (with results) or not</returns>
        internal async Task<ResponseModel> GetRatesData(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            string baseURL = $"http://rafasaints-001-site3.ctempurl.com";
            string controllerURL = $"/api/rates";

            ResponseModel result = new ResponseModel();

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            progress.Report(
                new ResponseModel()
                {
                    Message = $"Rates - Loading API data..."
                }
            );

            if (NetworkService.CheckConnectionTo(baseURL + controllerURL).IsSuccess)
            {
                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Rates - Connection successful... Downloading."
                    }
                );

                // If connection is made
                var APIresult = await GetJSONAPI(baseURL, controllerURL, progress, cToken);

                result.Result = JsonConvert.DeserializeObject<List<RatesModel>>((string)APIresult.Result);
                result.IsSuccess = true;

                stopwatch.Stop();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Rates - Download finished... {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                    }
                );
            }
            else
            {
                result.IsSuccess = false;
            }

            return result;
        }

        /// <summary>
        /// Gets data from the Hello Salut API. ("Hello" as a service)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="selectedCountry">The country to which one wants an acquire "Hello"</param>
        /// <returns>Response Model indicating if successful (with results) or not</returns>
        internal async Task<ResponseModel> GetHelloSalut(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel selectedCountry)
        {
            string baseURL = $"https://fourtonfish.com";
            string controllerURL = $"/hellosalut/?cc={selectedCountry.Alpha2Code}";

            ResponseModel result = new ResponseModel();

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            progress.Report(
                new ResponseModel()
                {
                    Message = $"Loading HelloSalut API data for {selectedCountry.Name}..."
                }
            );
            
            if (NetworkService.CheckConnectionTo(baseURL + controllerURL).IsSuccess)
            {
                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Connection successful... Downloading."
                    }
                );

                // If connection is made
                var APIresult = await GetJSONAPI(baseURL, controllerURL, progress, cToken);

                result.Result = JsonConvert.DeserializeObject<HellosalutModel>((string)APIresult.Result);
                result.IsSuccess = true;

                stopwatch.Stop();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Data Download finished... {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                    }
                );
            }
            else
            {
                result.IsSuccess = false;
            }

            return result;
        }

        /// <summary>
        /// Gets data from the Nager.Date API (Public holidays across the world)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="selectedCountry">The country to which one wants an acquire "Hello"</param>
        /// <returns>Response Model indicating if successful (with results) or not</returns>
        internal async Task<ResponseModel> GetNagerDate(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel selectedCountry)
        {
            string baseURL = $"http://date.nager.at";
            string controllerURL = $"/api/v1/get/{selectedCountry.Alpha2Code}/{DateTime.Now.Year}";

            ResponseModel result = new ResponseModel();

            var stopwatch = System.Diagnostics.Stopwatch.StartNew();

            progress.Report(
                new ResponseModel()
                {
                    Message = $"Loading Nager.Date API data for {selectedCountry.Name}..."
                }
            );

            if (NetworkService.CheckConnectionTo(baseURL + controllerURL).IsSuccess)
            {
                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Connection successful... Downloading."
                    }
                );

                // If connection is made
                var APIresult = await GetJSONAPI(baseURL, controllerURL, progress, cToken);

                result.Result = JsonConvert.DeserializeObject<List<NagerDateModel>>((string)APIresult.Result);
                result.IsSuccess = true;

                stopwatch.Stop();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Data Download finished... {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                    }
                );
            }
            else
            {
                result.IsSuccess = false;
            }

            return result;
        }
    }
}
