﻿namespace Infonations.Models.Country
{
    /// <summary>
    /// Model used for the countries coordinates
    /// </summary>
    public class CoordModel
    {
        /// <summary>
        /// Latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Longitude
        /// </summary>
        public double Longitude { get; set; }
    }
}
