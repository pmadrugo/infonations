﻿namespace Infonations.Models.Country
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Base model used for a country
    /// </summary>
    public class CountryModel
    {
        /// <summary>
        /// Country name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Top Level Domain - Internet suffix
        /// </summary>
        public List<string> TopLevelDomain { get; set; }

        /// <summary>
        /// Alpha 2 Code (ISO 3166)
        /// </summary>
        public string Alpha2Code { get; set; }

        /// <summary>
        /// Alpha 3 Code (ISO 3166)
        /// </summary>
        public string Alpha3Code { get; set; }

        /// <summary>
        /// List of a country's calling codes
        /// </summary>
        public List<string> CallingCodes { get; set; }

        /// <summary>
        /// Capital name
        /// </summary>
        public string Capital { get; set; }

        /// <summary>
        /// List of a country's alternative spellings
        /// </summary>
        public List<string> AltSpellings { get; set; }

        /// <summary>
        /// Object containing the country's region
        /// </summary>
        public RegionModel Region { get; set; }

        /// <summary>
        /// Object containing the country's subregion
        /// </summary>
        public SubregionModel Subregion { get; set; }

        /// <summary>
        /// The country's populations
        /// </summary>
        public long? Population { get; set; }

        /// <summary>
        /// Object containing the country's coordinates
        /// </summary>
        public CoordModel Coordinates { get; set; } //Latitude, Longitude

        /// <summary>
        /// Field containing the country's name for its people
        /// </summary>
        public string Demonym { get; set; }

        /// <summary>
        /// Field containing the country's area/size
        /// </summary>
        public double? Area { get; set; }

        /// <summary>
        /// Field containing the GINI index
        /// </summary>
        public double? GiniIndex { get; set; } // GINI Index

        /// <summary>
        /// List of TimeZoneModel objects representing the country's timezones
        /// </summary>
        public List<TimeZoneModel> TimeZones { get; set; } // TimeZones

        /// <summary>
        /// List of objects, initially containing the Alpha3Code of a country and post-assembly containing Country objects
        /// </summary>
        public List<object> BorderCountries { get; set; } // Countries that border this one

        /// <summary>
        /// Field containing the name given to the country by the very country
        /// </summary>
        public string Endonym { get; set; } // Native Name

        /// <summary>
        /// List of Currencies that the country uses
        /// </summary>
        public List<CurrencyModel> Currencies { get; set; } // List of currencies

        /// <summary>
        /// List of Languages that the country uses
        /// </summary>
        public List<LanguageModel> Languages { get; set; }

        /// <summary>
        /// Several translations of the country's name
        /// </summary>
        public TranslationModel Translations { get; set; }

        /// <summary>
        /// Path pointing to the SVG file containing the country's flag
        /// </summary>
        public string FlagPath { get; set; }

        /// <summary>
        /// List of Regional Blocs the country belongs to
        /// </summary>
        public List<RegionalBlocModel> RegionalBlocs { get; set; }

        /// <summary>
        /// Field containing the Code for the Internaional Olympic Comittee
        /// </summary>
        public string CIOC { get; set; } // Code International Olympic Comittee

        //Extras
        /// <summary>
        /// Field reserved for the string containing the word for hello in this country's main language
        /// </summary>
        public string Hello { get; set; }

        /// <summary>
        /// Field containing a list of the country's holidays
        /// </summary>
        public List<HolidayModel> Holidays { get; set; }

        /// <summary>
        /// Field containing the path a country's anthem file
        /// </summary>
        public string AnthemPath { get; set; }
    }
}
