﻿namespace Infonations.Models.Country
{
    using System.Collections.Generic;

    /// <summary>
    /// Object referring to a country's Regional Bloc
    /// </summary>
    public class RegionalBlocModel
    {
        /// <summary>
        /// The acronym for the regional bloc
        /// </summary>
        public string Acronym { get; set; }

        /// <summary>
        /// The name of the regional bloc
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// List of other acronyms that exist for the regional bloc
        /// </summary>
        public List<string> OtherAcronyms { get; set; }

        /// <summary>
        /// List of other names that the regional bloc has
        /// </summary>
        public List<string> OtherNames { get; set; }

        /// <summary>
        /// List of countries that belong to the regional bloc
        /// </summary>
        public List<CountryModel> Countries { get; set; }
    }
}