﻿namespace Infonations.Models.Country
{
    using System;

    /// <summary>
    /// Object containing a country's Timezone
    /// </summary>
    public class TimeZoneModel
    {
        /// <summary>
        /// The sign present in the timezone | True - Positive; Negative - False
        /// </summary>
        public bool Sign { get; set; } // Positive - True; Negative - False

        /// <summary>
        /// Unsigned time difference
        /// </summary>
        public TimeSpan BaseDifference { get; set; }

        /// <summary>
        /// Property that returns the calculated the time difference along with the sign. (Workaround negative Timespans with Negate)
        /// </summary>
        public virtual TimeSpan Difference
        {
            get
            {
                if (Sign)
                {
                    return BaseDifference;
                }
                else
                {
                    return BaseDifference.Negate();
                }
            }
        }

        /// <summary>
        /// Returns the current time at this timezone
        /// </summary>
        public virtual DateTime TZDateTimeNow
        {
            get { return DateTime.Now.Add(Difference); }
        }
        
        /// <summary>
        /// The timezone designation (ex: UTC +12:00)
        /// </summary>
        public string Designation { get; set; }
    }
}
