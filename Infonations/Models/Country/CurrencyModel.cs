﻿namespace Infonations.Models.Country
{
    using System.Collections.Generic;

    /// <summary>
    /// Country Currency Model
    /// </summary>
    public class CurrencyModel
    {
        /// <summary>
        /// Currency code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Currency name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Currency symbol
        /// </summary>
        public string Symbol { get; set; }

        /// <summary>
        /// Currency rate value for conversion (Euro as a reference)
        /// </summary>
        public double Rate { get; set; }
    }
}