﻿namespace Infonations.Models.Country
{
    /// <summary>
    /// Object containing the country's name in other languages
    /// </summary>
    public class TranslationModel
    {
        /// <summary>
        /// Country name in German
        /// </summary>
        public string German { get; set; }

        /// <summary>
        /// Country name in Spanish
        /// </summary>
        public string Spanish { get; set; }

        /// <summary>
        /// Country name in French
        /// </summary>
        public string French { get; set; }

        /// <summary>
        /// Country name in Japanese
        /// </summary>
        public string Japanese { get; set; }

        /// <summary>
        /// Country name in Italian
        /// </summary>
        public string Italian { get; set; }

        /// <summary>
        /// Country name in Brazillian
        /// </summary>
        public string Brazillian { get; set; }

        /// <summary>
        /// Country name in Portuguese
        /// </summary>
        public string Portuguese { get; set; }

        /// <summary>
        /// Country name in Dutch (Netherlands)
        /// </summary>
        public string Dutch { get; set; } // Netherlands

        /// <summary>
        /// Country name in Magyar (Hungary)
        /// </summary>
        public string Magyar { get; set; } // Hungary

        /// <summary>
        /// Country name in Farsi (Iran)
        /// </summary>
        public string Farsi { get; set; } // Iran
    }
}