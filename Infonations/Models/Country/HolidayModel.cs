﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infonations.Models.Country
{
    /// <summary>
    /// Base model for Holidays
    /// </summary>
    public class HolidayModel
    {
        /// <summary>
        /// Field for the holiday date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Field for the name of the holiday in the country's language
        /// </summary>
        public string LocalName { get; set; }

        /// <summary>
        /// Field for the english name of the holiday
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Field that indicates if the holiday is fixed throughout the years
        /// </summary>
        public bool IsFixed { get; set; }

        /// <summary>
        /// Field that indicates if the holiday is celebrated in other countries besides this one
        /// </summary>
        public bool IsGlobal { get; set; }

        /// <summary>
        /// The year when the holiday started being celebrated
        /// </summary>
        public string LaunchYear { get; set; }
    }
}
