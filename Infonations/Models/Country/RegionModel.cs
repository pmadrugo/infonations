﻿namespace Infonations.Models.Country
{
    using System.Collections.Generic;

    /// <summary>
    /// Object containing Region information used by countries
    /// </summary>
    public class RegionModel
    {
        /// <summary>
        /// The region's name
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// List of subregions within this region
        /// </summary>
        public List<SubregionModel> Subregions { get; set; }

        /// <summary>
        /// List of countries that are inside this region
        /// </summary>
        public List<CountryModel> Countries { get; set; }
    }
}
