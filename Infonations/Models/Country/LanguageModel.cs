﻿namespace Infonations.Models.Country
{
    using System.Collections.Generic;

    /// <summary>
    /// Language object containing information regarding a country's language
    /// </summary>
    public class LanguageModel
    {
        /// <summary>
        /// The language ISO 639-1 Code
        /// </summary>
        public string ISO639_1 { get; set; }

        /// <summary>
        /// The language ISO 639-2 Code
        /// </summary>
        public string ISO639_2 { get; set; }

        /// <summary>
        /// The language name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The language name in its own language
        /// </summary>
        public string Endonym { get; set; } // Native Name

        /// <summary>
        /// List of countries that speak this language
        /// </summary>
        public List<CountryModel> Countries { get; set; }
    }
}