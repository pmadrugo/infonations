﻿namespace Infonations.Models.Country
{
    using System.Collections.Generic;

    /// <summary>
    /// Object containing information regarding a Subregion
    /// </summary>
    public class SubregionModel
    {
        /// <summary>
        /// Name of the Subregion
        /// </summary>
        public string SubRegionName { get; set; }

        /// <summary>
        /// List of countries that belong to this subregion
        /// </summary>
        public List<CountryModel> Countries { get; set; }
    }
}
