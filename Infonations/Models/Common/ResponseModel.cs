﻿namespace Infonations.Models.Common
{
    /// <summary>
    /// Model used for Responses regarding several methods across the library
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Field used for reporting method success or not
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Field used for reporting a custom message sent by the method
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Field used to return a result from the method
        /// </summary>
        public object Result { get; set; }
    }
}