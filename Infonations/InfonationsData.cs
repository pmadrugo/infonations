﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infonations.Models.Country;
using Infonations.Services.Models.API.Rates;
using Infonations.Services.Models.API.RestCountries;
using Infonations.Services;
using System.Threading;
using Infonations.Models.Common;
using Infonations.Services.Models.API.Hellosalut;
using System.Net;
using Infonations.Services.Models.API.Nager.Date;
using System.Diagnostics;
using System.IO;
using System.ComponentModel;

namespace Infonations
{
    /// <summary>
    /// Main entry point for data retrieval
    /// </summary>
    public class InfonationsData
    {
        public string DatabasePath { get; set; }

        /// <summary>
        /// API Access Point
        /// </summary>
        public APIService APIService { get; set; }

        /// <summary>
        /// Database Access Point
        /// </summary>
        public DatabaseService DatabaseService { get; set; }

        /// <summary>
        /// List of post-assembly Country objects.
        /// </summary>
        public List<CountryModel> Countries { get; set; }

        /// <summary>
        /// List of post-assembly Region objects
        /// </summary>
        public List<RegionModel> Regions { get; set; }

        /// <summary>
        /// List of post-assembly Currency objects
        /// </summary>
        public List<CurrencyModel> Currencies { get; set; }

        /// <summary>
        /// List of post-assembly Regional Bloc objects
        /// </summary>
        public List<RegionalBlocModel> RegionalBlocs { get; set; }

        /// <summary>
        /// List of post-assembly Language objects
        /// </summary>
        public List<LanguageModel> Languages { get; set; }

        /// <summary>
        /// List of post-assembly Timezone objects
        /// </summary>
        public List<TimeZoneModel> TimeZones { get; set; }

        /// <summary>
        /// Main entry point for Infonations data
        /// </summary>
        public InfonationsData(string filePath)
        {
            this.DatabasePath = filePath;

            this.APIService = new APIService(DatabasePath);
            this.DatabaseService = new DatabaseService(DatabasePath);
        }

        /// <summary>
        /// Main method responsible for loading every piece of required data
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns></returns>
        public async Task LoadData(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            this.Regions = new List<RegionModel>();
            this.Currencies = new List<CurrencyModel>();
            this.RegionalBlocs = new List<RegionalBlocModel>();
            this.Languages = new List<LanguageModel>();
            this.TimeZones = new List<TimeZoneModel>();

            try
            {
                var restCountriesRequest = await this.APIService.GetRestCountriesData(progress, cToken);
                

                if (!restCountriesRequest.IsSuccess)
                {
                    restCountriesRequest = await this.DatabaseService.RestCountriesLoadAllFromDB();
                }
                else
                {
                    await this.DatabaseService.RestCountriesSaveAllToDB((List<RestCountriesModel>)restCountriesRequest.Result).ConfigureAwait(false);
                }

                var restCountries = (List<RestCountriesModel>)restCountriesRequest.Result;

                

                this.Countries = new List<CountryModel>(restCountries.Count);

                var stopwatch = System.Diagnostics.Stopwatch.StartNew();

                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Assembling Data..."
                    }
                );
                
                try
                {
                    SetupCountriesBaseObjects(progress, cToken, restCountries);

                    SetupCountriesAssemblyAsync(progress, cToken, restCountries);

                    SetupCountryBorders(progress, cToken);

                    await LoadData_Continuation(progress, cToken).ConfigureAwait(false);

                    stopwatch.Stop();

                    progress.Report(
                        new ResponseModel()
                        {
                            Message = $"Base Data assembled. {(double)(stopwatch.ElapsedMilliseconds / 1000)}s"
                        }
                    );
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Assembles the Regions, Subregions and Currencies acquired from the API
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="restCountries">List of objects containing raw data from the RestCountries API</param>
        /// <returns></returns>
        private void SetupCountriesBaseObjects(IProgress<ResponseModel> progress, CancellationToken cToken, List<RestCountriesModel> restCountries)
        {
            foreach (var restCountry in restCountries)
            {
                try
                {
                    // BEGIN Regions and Subregions
                    #region Regions and Subregions

                    if (!this.Regions.Any((region) => (region.RegionName == restCountry.region)))
                    {
                        this.Regions.Add(
                            new RegionModel()
                            {
                                Countries = new List<CountryModel>(),
                                RegionName = restCountry.region,
                                Subregions = new List<SubregionModel>()
                                {
                                    new SubregionModel()
                                    {
                                        Countries = new List<CountryModel>(),
                                        SubRegionName = restCountry.subregion
                                    }
                                }
                            }
                        );
                    }
                    else
                    {
                        var restCountryRegion = this.Regions.Where((region) => region.RegionName == restCountry.region).SingleOrDefault();
                        if (restCountryRegion != null)
                        {
                            if (!restCountryRegion.Subregions.Any((subregion) => subregion.SubRegionName == restCountry.subregion))
                            {
                                restCountryRegion.Subregions.Add(
                                    new SubregionModel()
                                    {
                                        Countries = new List<CountryModel>(),
                                        SubRegionName = restCountry.subregion
                                    }
                                );
                            }
                        }
                    }

                    #endregion
                    // END Regions and Subregions

                    // BEGIN Currencies
                    #region Currencies
                    foreach (var restCountryCurrency in restCountry.currencies)
                    {
                        if (!this.Currencies.Any((currency) => currency.Code == restCountryCurrency.code))
                        {
                            this.Currencies.Add(
                                new CurrencyModel()
                                {
                                    Code = restCountryCurrency.code,
                                    Name = restCountryCurrency.name,
                                    Symbol = restCountryCurrency.symbol
                                }
                            );
                        }
                    }
                    #endregion
                    // END Currencies

                    // BEGIN RegionalBlocs
                    #region Regional Blocs
                    foreach (var restCountryRegionalBloc in restCountry.regionalBlocs)
                    {
                        if (!this.RegionalBlocs.Any((regionalBloc) => regionalBloc.Name == restCountryRegionalBloc.name))
                        {
                            RegionalBlocModel regionalBlocToAdd = new RegionalBlocModel();

                            regionalBlocToAdd.Name = restCountryRegionalBloc.name;
                            regionalBlocToAdd.Acronym = restCountryRegionalBloc.acronym;

                            regionalBlocToAdd.OtherAcronyms = new List<string>();
                            if (restCountryRegionalBloc.otherAcronyms == null)
                            {
                                restCountryRegionalBloc.otherAcronyms = new List<RestCountriesRegionalBlocsOtherAcronymsModel>();
                            }
                            foreach (var restCountryOtherAcronym in restCountryRegionalBloc.otherAcronyms)
                            {
                                regionalBlocToAdd.OtherAcronyms.Add(restCountryOtherAcronym.OtherAcronym);
                            }
                            

                            regionalBlocToAdd.OtherNames = new List<string>();
                            if (restCountryRegionalBloc.otherNames == null)
                            {
                                restCountryRegionalBloc.otherNames = new List<RestCountriesRegionalBlocsOtherNamesModel>();
                            }
                            foreach (var restCountryOtherName in restCountryRegionalBloc.otherNames)
                            {
                                regionalBlocToAdd.OtherNames.Add(restCountryOtherName.OtherName);
                            }

                            regionalBlocToAdd.Countries = new List<CountryModel>();

                            this.RegionalBlocs.Add(regionalBlocToAdd);
                        }
                    }
                    #endregion
                    // END RegionalBlocs

                    // BEGIN Languages
                    #region Languages
                    foreach (var restCountryLanguage in restCountry.languages)
                    {
                        if (!this.Languages.Any((language) => language.ISO639_2 == restCountryLanguage.iso639_2))
                        {
                            this.Languages.Add(
                                new LanguageModel()
                                {
                                    Name = restCountryLanguage.name,
                                    ISO639_1 = restCountryLanguage.iso639_1,
                                    ISO639_2 = restCountryLanguage.iso639_2,
                                    Endonym = restCountryLanguage.nativeName,
                                    Countries = new List<CountryModel>()
                                }
                            );
                        }
                    }
                    #endregion
                    // END Languages

                    // BEGIN TimeZones
                    #region TimeZones
                    foreach (var restCountryTimeZone in restCountry.timezones)
                    {
                        if (!this.TimeZones.Any((timeZone) => timeZone.Designation == restCountryTimeZone.TimeZone))
                        {
                            if (restCountryTimeZone.TimeZone == "UTC")
                            {
                                this.TimeZones.Add(
                                    new TimeZoneModel()
                                    {
                                        Sign = true,
                                        BaseDifference = new TimeSpan(0, 0, 0),
                                        Designation = restCountryTimeZone.TimeZone
                                    }
                                );
                                continue;
                            }

                            var currentTimezone = restCountryTimeZone.TimeZone.Substring(3);

                            try
                            {
                                TimeZoneModel newTimeZone = new TimeZoneModel();

                                newTimeZone.Designation = restCountryTimeZone.TimeZone;
                                if (currentTimezone.Contains("+"))
                                {
                                    newTimeZone.Sign = true;
                                }
                                else
                                {
                                    newTimeZone.Sign = false;
                                }
                                currentTimezone = currentTimezone.Substring(1);

                                int hours;
                                int minutes;

                                if (currentTimezone.Contains(":"))
                                {
                                    hours = int.Parse(currentTimezone.Substring(0, currentTimezone.IndexOf(':')));
                                    minutes = int.Parse(currentTimezone.Substring(currentTimezone.IndexOf(':') + 1));
                                }
                                else
                                {
                                    hours = int.Parse(currentTimezone);
                                    minutes = 0;
                                }

                                newTimeZone.BaseDifference = new TimeSpan(hours, minutes, 0);

                                this.TimeZones.Add(newTimeZone);
                            }
                            catch (Exception ex)
                            {
                                Debug.WriteLine(ex.Message);
                            }
                        }
                    }
                    #endregion
                    // END TimeZones
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
            
        }

        /// <summary>
        /// Assembles the country objects
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="restCountries">List of objects containing raw data from the RestCountries API</param>
        /// <returns></returns>
        private void SetupCountriesAssemblyAsync(IProgress<ResponseModel> progress, CancellationToken cToken, List<RestCountriesModel> restCountries)
        {
            try
            {
                int countriesDone = 0;

                restCountries.AsParallel().ForAll(
                    (restCountry) =>
                    { 
                        try
                        {
                            CountryModel newCountry = new CountryModel();

                            // Alpha Codes
                            newCountry.Alpha2Code = restCountry.alpha2Code;
                            newCountry.Alpha3Code = restCountry.alpha3Code;

                            //Alt Spellings
                            newCountry.AltSpellings = new List<string>();
                            if (restCountry.altSpellings.Count > 0)
                            {
                                foreach (var restCountryAltSpelling in restCountry.altSpellings)
                                {
                                    newCountry.AltSpellings.Add(restCountryAltSpelling.AltSpelling);
                                }
                            }
                            else
                            {
                                newCountry.AltSpellings.Add("N/A");
                            }

                            // Area
                            newCountry.Area = restCountry.area;

                            // Calling Codes
                            newCountry.CallingCodes = new List<string>();
                            if (restCountry.callingCodes.Count > 0)
                            {
                                foreach (var restCountryCallingCode in restCountry.callingCodes)
                                {
                                    newCountry.CallingCodes.Add(restCountryCallingCode.CallingCode);
                                }
                            }
                            else
                            {
                                newCountry.CallingCodes.Add("N/A");
                            }

                            // Capital and CIOC
                            newCountry.Capital = restCountry.capital != "" ? restCountry.capital : "N/A";
                            newCountry.CIOC = restCountry.CIOC != "" ? restCountry.CIOC : "N/A";

                            // Coordinates
                            newCountry.Coordinates = new CoordModel()
                            {
                                Latitude = restCountry.latlng.latitude,
                                Longitude = restCountry.latlng.longitude
                            };

                            // Currencies
                            newCountry.Currencies = new List<CurrencyModel>();
                            if (restCountry.currencies.Count > 0)
                            {
                                foreach (var restCountryCurrency in restCountry.currencies)
                                {
                                    try
                                    {
                                        var currencyEntry = this.Currencies.Where((currency) => currency.Name == restCountryCurrency.name).SingleOrDefault();
                                        if (currencyEntry != null)
                                        {
                                            newCountry.Currencies.Add(currencyEntry);
                                        }
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                }
                            }

                            // Demonym, Endonym, FlagPath and GINI
                            newCountry.Demonym = restCountry.demonym != "" ? restCountry.demonym : "N/A";
                            newCountry.Endonym = restCountry.nativeName;
                            newCountry.FlagPath = restCountry.flag;
                            newCountry.GiniIndex = restCountry.gini;

                            // Languages
                            newCountry.Languages = new List<LanguageModel>();
                            if (restCountry.languages.Count > 0)
                            {
                                foreach (var restCountryLanguage in restCountry.languages)
                                {
                                    try
                                    {
                                        var languageEntry = this.Languages.Where((language) => language.ISO639_2 == restCountryLanguage.iso639_2).SingleOrDefault();
                                        if (languageEntry != null)
                                        {
                                            newCountry.Languages.Add(languageEntry);
                                        }
                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                }
                            }

                            // Name
                            newCountry.Name = restCountry.name;

                            // Population
                            newCountry.Population = restCountry.population;

                            // Regional Blocs
                            newCountry.RegionalBlocs = new List<RegionalBlocModel>();
                            if (restCountry.regionalBlocs.Count > 0)
                            {
                                foreach (var restcountryRegionalBlocs in restCountry.regionalBlocs)
                                {
                                    try
                                    {
                                        var regionalBlocEntry = this.RegionalBlocs.Where((regionalBloc) => regionalBloc.Name == restcountryRegionalBlocs.name).SingleOrDefault();

                                        if (regionalBlocEntry != null)
                                        {
                                            newCountry.RegionalBlocs.Add(regionalBlocEntry);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                            
                                }
                            }
                            if (newCountry.RegionalBlocs.Count == 0)
                            {
                                newCountry.RegionalBlocs.Add(
                                    new RegionalBlocModel()
                                    {
                                        Name = "N/A",
                                        Acronym = "N/A"
                                    }
                                );
                            }

                            // Top Level Domains
                            newCountry.TopLevelDomain = new List<string>();
                            if (restCountry.topLevelDomain.Count > 0)
                            {
                                foreach (var restCountryTopLevelDomain in restCountry.topLevelDomain)
                                {
                                    newCountry.TopLevelDomain.Add(restCountryTopLevelDomain.TopLevelDomain);
                                }
                            }
                            else
                            {
                                newCountry.TopLevelDomain.Add("N/A");
                            }

                            // Border Countries
                            newCountry.BorderCountries = new List<object>();
                            foreach (object restCountryBorder in restCountry.borders)
                            {
                                newCountry.BorderCountries.Add((RestCountriesBorderModel)restCountryBorder);
                            }

                            // Regions and Subregions
                            try
                            {
                                RegionModel regionEntry = this.Regions.Where((region) => region.RegionName == restCountry.region).SingleOrDefault();
                                SubregionModel subRegionEntry;

                                if (regionEntry != null)
                                {
                                    newCountry.Region = regionEntry;

                                    subRegionEntry = regionEntry.Subregions.Where((subRegion) => subRegion.SubRegionName == restCountry.subregion).SingleOrDefault();
                                    if (subRegionEntry != null)
                                    {
                                        newCountry.Subregion = subRegionEntry;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                newCountry.Region = null;
                                newCountry.Subregion = null;

                                throw new Exception(ex.Message, ex);
                            }


                            // TimeZones
                            newCountry.TimeZones = new List<TimeZoneModel>();
                            foreach (var restCountryTimeZone in restCountry.timezones)
                            {
                                try
                                {
                                    var currentTimezone = this.TimeZones.Where((timezone) => timezone.Designation == restCountryTimeZone.TimeZone).SingleOrDefault();

                                    if (currentTimezone != null)
                                    {
                                        newCountry.TimeZones.Add(currentTimezone);
                                    }
                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                            }

                            // Translations
                            newCountry.Translations = new TranslationModel()
                            {
                                Brazillian = restCountry.translations.br,
                                Dutch = restCountry.translations.nl,
                                Farsi = restCountry.translations.fa,
                                French = restCountry.translations.fr,
                                German = restCountry.translations.de,
                                Italian = restCountry.translations.it,
                                Japanese = restCountry.translations.ja,
                                Magyar = restCountry.translations.hr,
                                Portuguese = restCountry.translations.pt,
                                Spanish = restCountry.translations.es
                            };

                            // Add to List
                            lock (this.Countries)
                            {
                                this.Countries.Add(newCountry);
                                countriesDone++;

                                progress.Report(
                                    new ResponseModel()
                                    {
                                        Message = $"Loader|Loaded {newCountry.Name}",
                                        Result = (((double)this.Countries.Count / (double)restCountries.Count) * 100)
                                    }
                                );
                            }
                        }
                        catch (Exception ex)
                        {
                            progress.Report(
                                new ResponseModel()
                                {
                                    Message = ex.Message,
                                    Result = ex
                                }
                            );
                        }
                    }
                );
                
                foreach (var country in this.Countries)
                {
                    country.Region.Countries.Add(country);
                    country.Subregion.Countries.Add(country);

                    foreach (var regionalBloc in country.RegionalBlocs)
                    {
                        if (regionalBloc.Name != "N/A")
                        {
                            regionalBloc.Countries.Add(country);
                        }
                    }

                    foreach (var language in country.Languages)
                    {
                        language.Countries.Add(country);
                    }
                }

                foreach (RegionModel region in this.Regions)
                {
                    if (region.RegionName == "")
                    {
                        region.RegionName = "N/A";
                    }

                    foreach (SubregionModel subregion in region.Subregions)
                    {
                        if (subregion.SubRegionName == "")
                        {
                            subregion.SubRegionName = "N/A";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Sets up the countries borders, linking the values to other countries.
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns></returns>
        private void SetupCountryBorders(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            try
            {
                foreach (var country in this.Countries)
                {
                    if (country.BorderCountries != null)
                    {
                        List<object> borderCountries = new List<object>();
                        foreach (var borderCountry in country.BorderCountries)
                        {
                            if(!(borderCountry is  string)) { }
                            var currentCountry = this.Countries.Where((borderingCountry) => borderingCountry.Alpha3Code == ((RestCountriesBorderModel)borderCountry).Border).SingleOrDefault();
                            if (currentCountry != null)
                            {
                                borderCountries.Add(currentCountry);
                            }
                        }

                        country.BorderCountries = borderCountries.OrderBy((borderingCountry) => ((CountryModel)borderingCountry).Name).ToList();
                    };
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }

        }

        /// <summary>
        /// Calls the remaining initial setup data. (Rates for example)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <returns></returns>
        private async Task LoadData_Continuation(IProgress<ResponseModel> progress, CancellationToken cToken)
        {
            List<Task> APItasks = new List<Task>();
            
            // Rates API
            APItasks.Add(
                Task.Run(
                    async () =>
                    {
                        try
                        {
                            var ratesAPIRequest = await APIService.GetRatesData(progress, cToken);

                            if (!ratesAPIRequest.IsSuccess)
                            {
                                ratesAPIRequest = await this.DatabaseService.RatesLoadFromDB();
                            }
                            else
                            {
                                await this.DatabaseService.RatesSaveAllToDB((List<RatesModel>)ratesAPIRequest.Result).ConfigureAwait(false);
                            }

                            await GetRatesAPIData(progress, cToken, (List<RatesModel>)ratesAPIRequest.Result);
                        }
                        catch (Exception ex)
                        {
                            progress.Report(
                                new ResponseModel()
                                {
                                    IsSuccess = false,
                                    Message = ex.Message,
                                    Result = ex
                                }
                            );
                        }
                    }
                )
            );

            await Task.WhenAll(APItasks);

            progress.Report(
                new ResponseModel()
                {
                    Message = $"«« All data assembly completed. »»"
                }
            );
        }

        /// <summary>
        /// Method responsible for processing the deserialized data from the Rates API into the current models.
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="rates">List of Rates objects containing raw data from the Rates (Câmbios) API</param>
        /// <returns></returns>
        private async Task GetRatesAPIData(IProgress<ResponseModel> progress, CancellationToken cToken, List<RatesModel> rates)
        {
            int ratesCurrent = 0;

            var euroCurrency = this.Currencies.Where((eur) => eur.Code == "EUR").SingleOrDefault();

            if (euroCurrency != null)
            {
                euroCurrency.Rate = 1;
            }
            ratesCurrent++;

            var stopwatch = Stopwatch.StartNew();
            rates.AsParallel().ForAll(
                (APIrate) =>
                {
                var selectRate = (
                    from rate in this.Currencies
                    where rate.Code == APIrate.Code
                    select rate
                ).SingleOrDefault();

                if (selectRate != null)
                {
                    selectRate.Rate = APIrate.TaxRate;
                }

                ratesCurrent++;
                progress.Report(
                    new ResponseModel()
                    {
                        Message = $"Loader|Loaded Rate {(selectRate != null ? selectRate.Code : APIrate.Code)}",
                        Result = (((double)ratesCurrent / (double)rates.Count+1) * 100)
                    }
                    );
                }
            );
            stopwatch.Stop();
        }

        /// <summary>
        /// Gets the Hello text for the equivalent language (if possible)
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">The indicated country to which we want to know how to say hello</param>
        /// <returns>Response Model indicating if successful or not</returns>
        public async Task<ResponseModel> GetHelloData(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry)
        {
            if (String.IsNullOrEmpty(SelectedCountry.Hello))
            { 
                var hellosalutAPIResult = await this.APIService.GetHelloSalut(progress, cToken, SelectedCountry);

                if (hellosalutAPIResult.IsSuccess)
                {
                    if (!string.IsNullOrEmpty((((HellosalutModel)hellosalutAPIResult.Result).hello)))
                    {
                        await this.DatabaseService.HelloSalutUpdateDB((HellosalutModel)hellosalutAPIResult.Result);
                    }

                    var helloResult = (HellosalutModel)hellosalutAPIResult.Result;

                    SelectedCountry.Hello = WebUtility.HtmlDecode(helloResult.hello);

                    return new ResponseModel()
                    {
                        IsSuccess = true
                    };
                }
                else
                {

                    hellosalutAPIResult = this.DatabaseService.HelloSalutGetFromDB(SelectedCountry);

                    if (hellosalutAPIResult.IsSuccess)
                    {
                        SelectedCountry.Hello = ((HellosalutModel)(hellosalutAPIResult.Result)).hello;

                        return new ResponseModel()
                        {
                            IsSuccess = true
                        };
                    }
                    
                    return new ResponseModel()
                    {
                        IsSuccess = false
                    };
                }
            }

            return new ResponseModel()
            {
                IsSuccess = true,
                Message = $"Country already contains Hello message"
            };
        }

        /// <summary>
        /// Gets the Nager.Date holidays data for the specified country
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">The indicated country to which we want to know the holidays</param>
        /// <returns>Response Model indicating if successful or not</returns>
        public async Task<ResponseModel> GetNagerDateAPIData(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry)
        {
            try
            {
                if (SelectedCountry.Holidays == null)
                {
                    var nagerDateAPIResult = await this.APIService.GetNagerDate(progress, cToken, SelectedCountry);

                    if (nagerDateAPIResult.IsSuccess)
                    {
                        var nagerAPI = (List<NagerDateModel>)nagerDateAPIResult.Result;

                        await this.DatabaseService.NagerDateUpdateDB((List<NagerDateModel>)nagerAPI, SelectedCountry);

                        SelectedCountry.Holidays = new List<HolidayModel>();
                        
                        nagerAPI.AsParallel().ForAll(
                            (holiday) =>
                            {
                                var newHoliday = new HolidayModel()
                                {
                                    Date = DateTime.Parse(holiday.date),
                                    IsFixed = holiday.@fixed,
                                    IsGlobal = holiday.global,
                                    LaunchYear = holiday.launchYear != null ? holiday.launchYear.ToString() : $"-",
                                    LocalName = holiday.localName,
                                    Name = holiday.name.Replace("\t", "")
                                };

                                lock (SelectedCountry.Holidays)
                                {
                                    SelectedCountry.Holidays.Add(newHoliday);
                                }
                            }
                        );

                        SelectedCountry.Holidays = SelectedCountry.Holidays.OrderBy((holiday) => holiday.Date.Year)
                                                        .ThenBy((holiday) => holiday.Date.Month)
                                                        .ThenBy((holiday) => holiday.Date.Day)
                                                    .ToList();

                        return new ResponseModel()
                        {
                            IsSuccess = true
                        };
                    }
                    else
                    {
                        nagerDateAPIResult = this.DatabaseService.NagerDateGetFromDB(SelectedCountry);

                        if (nagerDateAPIResult.IsSuccess)
                        {
                            var nagerAPI = (List<NagerDateModel>)nagerDateAPIResult.Result;

                            SelectedCountry.Holidays = new List<HolidayModel>();

                            List<Task> nagerAPITasks = new List<Task>();

                            foreach (NagerDateModel holiday in nagerAPI)
                            {
                                nagerAPITasks.Add(
                                    Task.Run(
                                        () =>
                                        {
                                            SelectedCountry.Holidays.Add(
                                                new HolidayModel()
                                                {
                                                    Date = DateTime.Parse(holiday.date),
                                                    IsFixed = holiday.@fixed,
                                                    IsGlobal = holiday.global,
                                                    LaunchYear = holiday.launchYear != null ? holiday.launchYear.ToString() : $"-",
                                                    LocalName = holiday.localName,
                                                    Name = holiday.name.Replace("\t", "")
                                                }
                                            );
                                        }
                                    )
                                );
                            }

                            await Task.WhenAll(nagerAPITasks);

                            SelectedCountry.Holidays = SelectedCountry.Holidays.OrderBy((holiday) => holiday.Date.Year)
                                                            .ThenBy((holiday) => holiday.Date.Month)
                                                            .ThenBy((holiday) => holiday.Date.Day)
                                                        .ToList();

                            return new ResponseModel()
                            {
                                IsSuccess = true
                            };
                        }

                        return new ResponseModel()
                        {
                            IsSuccess = false
                        };
                    }
                }

                return new ResponseModel()
                {
                    IsSuccess = true,
                    Message = $"Country already contains Holidays data"
                };
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Gets the anthem for the specified country
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">The indicated country to which we want get the anthem</param>
        /// <param name="onDownloadComplete">[Delegate] Action to perform when download completes</param>
        /// <returns>Response Model indicating if successful or not</returns>
        public static ResponseModel GetCountryAnthem(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry, AsyncCompletedEventHandler onDownloadComplete)
        {
            string nationalAnthemURL = $"http://www.nationalanthems.info/";
            var localAnthem = CheckIfAnthemExists(progress, cToken, SelectedCountry);

            if (!localAnthem.IsSuccess)
            {
                var webURL = nationalAnthemURL+SelectedCountry.Alpha2Code.ToLower()+".mp3";
                if (NetworkService.CheckConnectionTo(webURL).IsSuccess)
                {
                    try
                    {
                        using (WebClient webClient = new WebClient())
                        {
                            webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(onDownloadComplete);
                            webClient.DownloadFileAsync(new Uri(webURL), $@"Anthems\{SelectedCountry.Alpha2Code}.mp3");
                        }

                        SelectedCountry.AnthemPath = $@"Anthems\{SelectedCountry.Alpha2Code}.mp3";
                        return new ResponseModel()
                        {
                            IsSuccess = true
                        };
                    }
                    catch (Exception ex)
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = false,
                            Message = ex.Message,
                            Result = ex
                        };
                    }
                }
                else
                {
                    if (NetworkService.CheckConnectionTo(nationalAnthemURL + SelectedCountry.Alpha3Code + ".mp3").IsSuccess)
                    {
                        webURL = nationalAnthemURL+SelectedCountry.Alpha3Code.ToLower()+".mp3";

                        try
                        {
                            using (WebClient webClient = new WebClient())
                            {
                                webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(onDownloadComplete);
                                webClient.DownloadFileAsync(new Uri(webURL), $@"Anthems\{SelectedCountry.Alpha3Code}.mp3");
                            }

                            SelectedCountry.AnthemPath = $@"Anthems\{SelectedCountry.Alpha3Code}.mp3";
                            return new ResponseModel()
                            {
                                IsSuccess = true
                            };
                        }
                        catch (Exception ex)
                        {
                            return new ResponseModel()
                            {
                                IsSuccess = false,
                                Message = ex.Message,
                                Result = ex
                            };
                        }
                    }
                    else
                    {
                        return new ResponseModel()
                        {
                            IsSuccess = false
                        };
                    }
                }
            }
            else
            {
                return new ResponseModel()
                {
                    IsSuccess = true,
                    Result = localAnthem.Result
                };
            }
        }

        /// <summary>
        /// Checks if the anthem was already downloaded
        /// </summary>
        /// <param name="progress">Progress object to communicate outside this method</param>
        /// <param name="cToken">Cancellation Token</param>
        /// <param name="SelectedCountry">The indicated country to which we want get the anthem</param>
        /// <returns>Response Model indicating if it was successful or not and its current path</returns>
        private static ResponseModel CheckIfAnthemExists(IProgress<ResponseModel> progress, CancellationToken cToken, CountryModel SelectedCountry)
        {
            string path = $@"Anthems\{SelectedCountry.Alpha2Code}.mp3";
            string path2 = $@"Anthems\{SelectedCountry.Alpha3Code}.mp3";

            Directory.CreateDirectory($@"Anthems");

            if (new FileInfo(path).Exists)
            {
                return new ResponseModel()
                {
                    IsSuccess = true,
                    Result = path
                };
            }
            if (new FileInfo(path2).Exists)
            {
                return new ResponseModel()
                {
                    IsSuccess = true,
                    Result = path
                };
            }
            return new ResponseModel()
            {
                IsSuccess = false
            };
        }
    }
}
